#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "chartwidget.h"
#include "vsectionfield.h"
#include <QtCharts>
#include <QString>
#include <QFile>
#include <QFileDialog>
#include <QPrinter>
#include <QPrintDialog>
#include <QImage>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	initAxesEditor();

	// handle spinBox value change
	connect(this->ui->rowColumnNumberSpinBox,SIGNAL(valueChanged(int)), this, SLOT(plotSection()));

	// handle section mode change
	connect(this->ui->rowSectionRadioButton, SIGNAL(toggled(bool)), this, SLOT(plotSection()));
	connect(this->ui->columnSectionRadioButton, SIGNAL(toggled(bool)), this, SLOT(plotSection()));

	// hadler for open CSV button clicked
	connect(this->ui->open_file_button,SIGNAL(clicked(bool)), this, SLOT(open_file_buttonClicked()));
	connect(this->ui->chartWidget, &ChartWidget::replot, this, &MainWindow::replot);

	connect(this->ui->chartWidget, SIGNAL(fileChanged()), this, SLOT(plotSection()));

	connect(this, &MainWindow::destroyed, this->ui->chartWidget, &ChartWidget::saveSettings);

	// interpolation icon
	QIcon interpolationIcon = QIcon(":/images/interpolation.png");

	//set interpolation icon and tool tip
	ui->actionInterpolated_graph->setIcon(interpolationIcon);
	ui->actionInterpolated_graph->setToolTip(tr("Interpolation using cubic spline"));

	// intersection widths icon
	QIcon intersectionsIcon = QIcon(":/images/intersection.png");

	// set window icon as interpolation icon
	setWindowIcon(interpolationIcon);
	setWindowTitle(tr("SAR spatial resolution estimator"));

	//set intersection widths icon and tool tip
	ui->actionIntersection_widths->setIcon(intersectionsIcon);
	ui->actionIntersection_widths->setToolTip(tr("Intersection widths"));

	QIcon axesEditorIcon = QIcon(":/images/axes_editor.png");

	ui->actionOpen_axes_editor->setIcon(axesEditorIcon);
	ui->actionOpen_axes_editor->setToolTip(tr("Axes editor"));

	QIcon sincIcon = QIcon(":/images/sinc.png");

	ui->actionSinc_approximation->setIcon(sincIcon);
	ui->actionSinc_approximation->setToolTip(tr("Approximation by sinc function"));



	ui->vSectionWidget->setContentLayout(makeMaxTableLayout());

	ui->widthCalculatorParameterLineEdit->setValidator(new QDoubleValidator(1, DBL_MAX, 0));
	ui->speedLineEdit->setValidator(new QDoubleValidator(0.01, 3000, 2));

	ui->widthCalculatorParameterLineEdit->setText(QString::number(ui->chartWidget->getCalculatorParam("ADC frequency"),'f',0));
	ui->widthCalculatorParameterLineEdit->setText(QString::number(ui->chartWidget->getCalculatorParam("PFR"),'f',0));
	ui->speedLineEdit->setText(QString::number(ui->chartWidget->getCalculatorParam("speed")));

	if (QString(KSI).size() > 0) {
		setWindowIcon(sincIcon);
		ui->actionInterpolated_graph->setIcon(sincIcon);
		ui->actionSinc_approximation->setVisible(false);
		ui->menuSettings->menuAction()->setVisible(false);
		ui->menuSettings->setVisible(false);
	}
}

MainWindow::~MainWindow()
{
	delete ui;
}

// standart resizeEvent + emit sizeChanged() signal for chartWidget
void MainWindow::resizeEvent(QResizeEvent *event){
	QMainWindow::resizeEvent(event);
	emit ui->chartWidget->sizeChanged();
}

QLayout& MainWindow::makeMaxTableLayout()
{
	auto * sectionLayout = new QVBoxLayout();
	QTableWidget* contentTable = new QTableWidget();
	sectionLayout->addWidget(contentTable);
	contentTable->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	contentTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
	contentTable->setShowGrid(false);
	contentTable->setSelectionBehavior(QAbstractItemView::SelectRows);
	contentTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	contentTable->verticalHeader()->setVisible(false);
	contentTable->horizontalHeader()->setSectionResizeMode (QHeaderView::Fixed);

	updateMaxTable(contentTable);

	connect(ui->chartWidget, &ChartWidget::sectionChanged, [contentTable, this] ()
	{
		updateMaxTable(contentTable);
	});
	connect(ui->chartWidget, &ChartWidget::sizeChanged, [contentTable, this] ()
	{
		updateMaxTable(contentTable);
	});
	connect(contentTable, &QTableWidget::cellDoubleClicked, [contentTable, this](int row, int)
	{
		QString text = contentTable->item(row, 0)->text();
		ui->rowColumnNumberSpinBox->setValue(std::atoi(text.toStdString().c_str()));
	});

	return *sectionLayout;
}

void MainWindow::updateMaxTable(QTableWidget *contentTable)
{
	bool row = ui->rowSectionRadioButton->isChecked();
	QList<QPair<int, double>> values = ui->chartWidget->getMaxValues(row);
	if (values.size() == 0)
		return;
	contentTable->hide();
	contentTable->clearContents();
	contentTable->setColumnCount(2);
	contentTable->setRowCount(values.length());

	for (int i = 0; i < values.length(); i++) {
		QTableWidgetItem *cell = contentTable->item(i, 0);
		if (!cell)
			cell = new QTableWidgetItem();
		cell->setTextAlignment(Qt::AlignCenter);
		contentTable->setItem(i, 0, cell);
		contentTable->item(i, 0)->setText(QString::number(values[i].first));

		cell = contentTable->item(i, 1);
		if (!cell)
			cell = new QTableWidgetItem();
		cell->setTextAlignment(Qt::AlignCenter);
		contentTable->setItem(i, 1, cell);
		contentTable->item(i, 1)->setText(QString::number(values[i].second));
	}

	contentTable->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Row")));
	if (!row)
		contentTable->horizontalHeaderItem(0)->setText(QString(tr("Column")));
	contentTable->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Value")));

	// It is necessary to use fixed values instead of contantTable->width()
	// because when animation changes state to "Stopped" contantTable->width()
	// for some reason can be from 220 to 254(max). Don't know why.
	contentTable->setColumnWidth(0, 100);
	// 2nd column width: 254 (-100 for 1st column)  (-15) for vertical scroll bar (-2) for borders
	contentTable->setColumnWidth(1, 154 - 15 - 2);
	// If vertical scroll bar is not visible (low amount af values)
	// Tryed to check if vertical scroll bar is visible, but always got "false"
	if (contentTable->rowHeight(0)*values.size() <= contentTable->height())
		contentTable->setColumnWidth(1, 154 - 2);

	contentTable->show();
}

void MainWindow::replot(){
	ui->chartWidget->setContourLine(ui->contourLineSpinBox->value());
	if (ui->actionInterpolated_graph->isChecked())
		ui->chartWidget->setInterpolatedDataVisible(true);
	if (ui->actionIntersection_widths->isChecked())
		ui->chartWidget->setIntersectionWidthsVisible(true);
	if (ui->actionSinc_approximation->isChecked())
		ui->chartWidget->setSincSeriesVisible(true);

	ui->chartWidget->setSectionVisible(ui->actionSection_graph->isChecked());
}

// open file button clicked
void MainWindow::open_file_buttonClicked()
{
	ui->chartWidget->loadData();
}

// plot current data section
void MainWindow::plotSection()
{
	// get current data size of widget
	QSize temp = ui->chartWidget->dataSize();

	// if ccurrent data size = (0,0), do nothing
	if (temp.height() == 0 || temp.width() == 0)
		return;

	ui->widthCalculatorParameterLineEdit->setEnabled(true);
	ui->speedLineEdit->setEnabled(true);

	// if row radio button checked, set section by row
	// and set new maximum for row/column number spin box
	if (ui->rowSectionRadioButton->isChecked()) {
		ui->sectionByLabel->setText(tr("Row:"));
		ui->widthCalculatorParameterLabel->setText(tr("ADC frequency, Hz"));
		ui->widthCalculatorParameterLineEdit->setText(QString::number(ui->chartWidget->getCalculatorParam("ADC frequency"),'f',0));
		ui->speedLabel->setVisible(false);
		ui->speedLineEdit->setVisible(false);
		ui->rowColumnNumberSpinBox->setMaximum(temp.height() - 1);
		ui->chartWidget->sectionByRow(ui->rowColumnNumberSpinBox->value());
	} else {
		ui->sectionByLabel->setText(tr("Column:"));
		ui->widthCalculatorParameterLabel->setText(tr("PFR, Hz"));
		ui->widthCalculatorParameterLineEdit->setText(QString::number(ui->chartWidget->getCalculatorParam("PFR"),'f',0));
		ui->speedLabel->setVisible(true);
		ui->speedLineEdit->setVisible(true);
		ui->rowColumnNumberSpinBox->setMaximum(temp.width() - 1);
		ui->chartWidget->sectionByColumn(ui->rowColumnNumberSpinBox->value());
	}
}

void MainWindow::on_decibelCheckBox_clicked(bool checked)
{
	// set calculation in decibels (true) / (false) do not use decibels
	ui->chartWidget->setDecibelScale(checked);
	ui->chartWidget->hideAllIntersectionLabels();
	ui->chartWidget->setBasis(ui->basisSpinBox->value());
	ui->basisSpinBox->setEnabled(checked);

	// hide intersection widths
	ui->chartWidget->setIntersectionWidthsVisible(false);

	// disable and hide contour line and its checkbox and spinbox
	ui->chartWidget->setContourLineVisible(!checked);
	ui->contourLineSpinBox->setEnabled(!checked);
	ui->contourLineCheckBox->setEnabled(!checked);
	ui->chartWidget->setContourLineVisible(!checked && ui->contourLineCheckBox->isChecked());

	// disable intersection width button and menu
	ui->actionIntersection_widths->setEnabled(!checked);
	ui->actionIntersection_widths->setChecked(false);
	ui->menuIntersection_widths_labels->setEnabled(!checked);

	// disable sinc approximation
	ui->actionSinc_approximation->setEnabled(checked);
	ui->actionSinc_approximation->setChecked(false);
}

void MainWindow::on_saveChartAs_triggered()
{
	ui->chartWidget->saveAs();
}

void MainWindow::on_actionPrint_triggered()
{
	ui->chartWidget->print();
}

void MainWindow::on_basisSpinBox_valueChanged(double arg1)
{
	ui->chartWidget->setBasis(arg1);
	MainWindow::plotSection();
}

void MainWindow::on_contourLineSpinBox_valueChanged(double arg1)
{
	ui->chartWidget->setContourLine(arg1);
	ui->chartWidget->setIntersectionWidthsVisible(ui->actionIntersection_widths->isChecked());
}

void MainWindow::on_actionAntialiasing_triggered()
{
	bool state = ui->actionAntialiasing->isChecked();
	ui->chartWidget->setRenderHint(QPainter::Antialiasing, state);
}

void MainWindow::on_actionInterpolated_graph_triggered()
{
	bool state = ui->actionInterpolated_graph->isChecked();
	ui->chartWidget->setInterpolatedDataVisible(state);
}

void MainWindow::on_actionSection_graph_triggered()
{
	bool state = ui->actionSection_graph->isChecked();
	ui->chartWidget->setSectionVisible(state);
}

void MainWindow::on_actionIntersection_widths_triggered()
{
	bool state = ui->actionIntersection_widths->isChecked();
	ui->chartWidget->setIntersectionWidthsVisible(state);
}

void MainWindow::on_contourLineCheckBox_clicked(bool checked)
{
	if (checked)
		ui->chartWidget->setContourLine(ui->contourLineSpinBox->value());
	ui->chartWidget->setContourLineVisible(checked);
}

void MainWindow::on_actionShow_all_Width_Labels_triggered()
{
	if (ui->actionIntersection_widths->isChecked())
		ui->chartWidget->showAllIntersectionLabels();
}

void MainWindow::on_actionHide_all_Widths_Labels_triggered()
{
	ui->chartWidget->hideAllIntersectionLabels();
}

void MainWindow::on_actionOpenFile_triggered()
{
	ui->chartWidget->loadData();
}

void MainWindow::on_actionTable_of_sorted_values_toggled(bool arg1)
{
	ui->vSectionWidget->setVisible(arg1);
	ui->actionTable_of_sorted_values->setChecked(arg1);
}

void MainWindow::on_actionOpen_axes_editor_triggered()
{
	axesEditor->show();
}

void MainWindow::initAxesEditor()
{
	axesEditor = ui->chartWidget->getAxesEditWidget();
	axesEditor->setWindowTitle(tr("Axes editor"));
	axesEditor->resize(450, 175);
	axesEditor->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void MainWindow::on_actionSinc_approximation_triggered()
{
	bool state = ui->actionSinc_approximation->isChecked();
	ui->chartWidget->setSincSeriesVisible(state);
}

void MainWindow::on_widthCalculatorParameterLineEdit_editingFinished()
{
	double val = ui->widthCalculatorParameterLineEdit->text().toDouble();
	if (ui->rowSectionRadioButton->isChecked()) {
		ui->chartWidget->setCalculatorParam("ADC frequency", val);
	} else {
		ui->chartWidget->setCalculatorParam("PFR", val);
	}
	ui->chartWidget->intersectionWidthsCalculationParamsChanged();
	ui->widthCalculatorParameterLineEdit->clearFocus();
}

void MainWindow::on_speedLineEdit_editingFinished()
{
	double val = ui->speedLineEdit->text().toDouble();
	ui->chartWidget->setCalculatorParam("speed", val);
	ui->chartWidget->intersectionWidthsCalculationParamsChanged();
	ui->speedLineEdit->clearFocus();
}

void MainWindow::on_actionChangeLanguageRussian_triggered()
{
	changeLanguage("ru");
}

void MainWindow::on_actionChangeLanguageEnglish_triggered()
{
	changeLanguage("");
}
void MainWindow::changeLanguage(QString language)
{
	QSettings settings(QSettings::IniFormat, QSettings::UserScope, "zptel", "PlotFromCSV");
	QString currentLanguage = settings.value("Language", "").toString();
	if (currentLanguage == language)
		return;
	settings.setValue("Language", QVariant(language));
	auto changeLanguageMessageBox = std::unique_ptr<QMessageBox>(new QMessageBox);
	changeLanguageMessageBox->setText(tr("You need to restart the application to apply changes."));
	changeLanguageMessageBox->setWindowTitle(tr("Language settings changed"));
	changeLanguageMessageBox->exec();
}
