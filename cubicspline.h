#ifndef CUBICSPLINE_H
#define CUBICSPLINE_H
#include <QObject>
#include <QVector>

/* Class CubicSpline provides ability to interpolate array of knot points by
 * cubic spline.
 * You can get interpolated values using function getValuesFromTo.
 * Also you can find roots at segment using solveSplineAtSegment method.
 * Description of functions in source file.
 */
class CubicSpline
{
public:
	CubicSpline(){}
	CubicSpline(const QList<QPointF> &points, bool calc_coeffs = true);

	~CubicSpline();

	int length() const;
	int segmentOf(double x) const;
	double operator ()(double x) const;

	QPointF knotAt(int position);
	QPointF at(double x) const;

	QList<QVector<double>> splineCoeffs();
	QList<QPointF> solveAtSegment(double value, int position);
	QList<QPointF> interpolate(double fromX, double toX, double step);
	QList<QPointF> knotPoints();
	QVector<double> accuracy();

	void setPoints(const QList<QPointF> points);

private:
	bool m_interpolated;

	QVector<double> m_roots_accuracy;
	QVector<double> m_a;
	QVector<double> m_b;
	QVector<double> m_c;
	QVector<double> m_d;

	QList<QPointF> m_knot_points;

    template<class T>
    int sgn(T value);

    void calculateSplineCoeffs();
};
#endif // CUBICSPLINE_H
