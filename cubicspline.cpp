#include "cubicspline.h"
#include <QPointF>
#include <QDebug>
#include <qmath.h>

#include "interpolationassistant.h"

CubicSpline::CubicSpline(const QList<QPointF> &points, bool calc_coeffs)
{
	m_knot_points = points;
	if (calc_coeffs)
		calculateSplineCoeffs();
}

CubicSpline::~CubicSpline(){}


// returns point from list of initial points at position "position"
QPointF CubicSpline::knotAt(int position)
{
	if (m_knot_points.isEmpty()){
		qDebug()<<"Spline is empty. Please, initialize spline with minimum 2 points";
        return QPointF();
    }

	return m_knot_points[position];
}

// returns length of list of initial points
int CubicSpline::length() const
{
	return m_knot_points.length();
}

// returns number of segment which contains "x" (binary search method)
int CubicSpline::segmentOf(double x) const
{
	bool end;
	int N = length() - 1;
	int n = N/2.0;
	int counter =  n;

	do{
		if (x < m_knot_points[counter].x()){
			counter = counter - n/2.0;
		}
		else{
			counter = counter + n/2.0;
		}
		// do not let n be zero. minimum step = 1
		n = n/2.0 > 0? n/2.0 : 2;

		if (counter > N/2){
			end = m_knot_points[counter + 1].x() < x;
			end = end ? end : m_knot_points[counter].x() > x;
		}
		else{
			end = m_knot_points[counter].x() > x;
			end = end ? end : m_knot_points[counter + 1].x() < x;
		}
	}
	while(end);

	return counter;
}

// returns interpolated cubic spline value at coordinate "x"
QPointF CubicSpline::at(double x) const {

    // if list of initial points is empty, return empty point
	if (m_knot_points.isEmpty()){
		qDebug()<<"Spline is emplty. Please, initialize spline with minimum 2 points";
        return QPointF();
    }

	// if "x" <= x coordinate of first initial point, return first initial point
	if (x <= m_knot_points.first().x()){
		return m_knot_points.first();
    }

	// if "x" >= x coordinate of last initialpoint, return last initial point
	if (x >= m_knot_points.last().x()){
		return m_knot_points.last();
    }

	int n = segmentOf(x);
	double dx =  x - m_knot_points[n].x();
	double func = m_a[n] + m_b[n]*dx + m_c[n]*pow(dx,2) + m_d[n]*pow(dx,3);

    return QPointF(x, func);
}

double CubicSpline::operator ()(double x) const {
	double res = at(x).y();
	return res;
}

// returns real roots of cubic equation at segment number "position"
// a + b(x-xi) + c(x-xi)^2 + d(x-xi)^3 = "value"
QList<QPointF> CubicSpline::solveAtSegment(double value, int position)
{

	QList<QPointF> roots;

	double x = m_knot_points[position].x();
	int n = position;

    // solutions
    double x1 = NAN;
    double x2 = NAN;
    double x3 = NAN;

    // equation coefficients
	double p0 = (m_a[n] - m_b[n]*x + m_c[n]*pow(x,2) - m_d[n]*pow(x,3) - value) / m_d[n];
	double p1 = (m_b[n] - 2*m_c[n]*x + 3*m_d[n]*pow(x,2)) / m_d[n];
	double p2 = (m_c[n] - 3*m_d[n]*x) / m_d[n];

    // Viet's-Cardano method
    double Q = (pow(p2,2.0) - 3*p1) / 9;
    double R = (2*pow(p2,3.0) - 9*p2*p1 + 27*p0) / 54;

    if (pow(R,2.0) < pow(Q,3.0)){
        double phi = acos( R / sqrt(pow(Q,3.0)) ) / 3;

        // calculating roots
        x1 = -2*sqrt(Q)*cos(phi) - p2/3;
        x2 = -2*sqrt(Q)*cos(phi + 2*M_PI/3.0) - p2/3;
        x3 = -2*sqrt(Q)*cos(phi - 2*M_PI/3.0) - p2/3;
    }
    else{
		double A = -sgn(R) * pow(qAbs(R) + sqrt(pow(R,2.0) - pow(Q,3.0)), 1.0/3.0);

        // B = Q/A. if A = 0, B = 0
        double B = A != 0? Q/A : 0;
        x1 = A + B - p2/3;
    }
    // only real roots calculated

    QPointF point;
	double accuracy = value/1000;

    // if x1 calculated and in knotPoints limits, get interpolated data at x1
	if (x1 != NAN && x1 >= m_knot_points.first().x() && x1 <= m_knot_points.last().x()){
		point = at(x1);
		// if interpolated data is near contour line, append it to intersention points
        if (qAbs(point.y() - value) < accuracy){
            roots.append(point);
			m_roots_accuracy.append(qAbs(point.y() - value));
        }
    }

    // if x2 calculated and >= 0, try to get interpolated data at x2
	if (x2 != NAN && x2 >= m_knot_points.first().x() && x2 <= m_knot_points.last().x()){
		point = at(x2);

		// if interpolated data is near contour line, append it to intersention points
        if (qAbs(point.y() - value) < accuracy){
            roots.append(point);
			m_roots_accuracy.append(qAbs(point.y() - value));
        }
    }

    // if x3 calculated and >= 0, try to get interpolated data at x3
	if (x3 != NAN && x3 >= m_knot_points.first().x() && x3 <= m_knot_points.last().x()){
		point = at(x3);

		// if interpolated data is near contour line, append it to intersention points
        if (qAbs(point.y() - value) < accuracy){
            roots.append(point);
			m_roots_accuracy.append(qAbs(point.y() - value));
        }
    }

    return roots;
}

// returns spline coefficients
QList<QVector<double>> CubicSpline::splineCoeffs()
{
	QList<QVector<double>> spline_coeffs;
	spline_coeffs.push_back(m_a);
	spline_coeffs.push_back(m_b);
	spline_coeffs.push_back(m_c);
	spline_coeffs.push_back(m_d);
	return spline_coeffs;
}

// returns list of cubic spline values at segment [fromX, toX] with step "step"
QList<QPointF> CubicSpline::interpolate(double fromX, double toX, double step){

    // return value
    QList<QPointF> interpolatedValues;

	if (m_knot_points.isEmpty() || m_knot_points.length() == 1){
		qDebug()<<"Spline is empty. Please, initialize spline with minimum 2 points";
        return interpolatedValues;
    }

    // check fromX is valid
	double startX = m_knot_points.first().x();

	if (fromX < startX) {
		qDebug()<<"Starting coordinate is too low. Changed to coordinate of first knot point.";
	} else {
        startX = fromX;
    }

    // check toX is valid
	double endX = m_knot_points.last().x();

	// if toX > x coordinate of last initial point, sets toX = x coordinate of last initial point
    if (toX > endX){
		qDebug()<<"Ending coordinate is too big. Changed to coordinate of last knot point.";
	} else {
        endX = toX;
    }

    // check step is valid
    if (step > (toX - fromX)){
        qDebug()<<"Too big step.";
        return interpolatedValues;
    }


	double func = 0;	// function value
	double dx = 0 ;		// x - xi
	double x = 0;
	int n = segmentOf(startX);		// number of start segment
	int N = (endX - startX)/step;	// number of steps

	// interpolate segments
	for(int i = 0; i < N; i++){
		x = startX + i*step;
		if (n + 1 < length() && m_knot_points[n + 1].x() <= x)
			n++;
		dx =  x - m_knot_points[n].x();
		func = m_a[n] + m_b[n]*dx + m_c[n]*pow(dx,2) + m_d[n]*pow(dx,3);

        interpolatedValues.append(QPointF(x,func));
    }

	// loop can do not get last point with step = "step"
	// and graph will be unfinished, so just append last point
	if (endX == m_knot_points[length() - 1].x() &&
		interpolatedValues[interpolatedValues.size() - 1].x() != endX)
		interpolatedValues.push_back(m_knot_points[length() - 1]);

	return interpolatedValues;
}

QList<QPointF> CubicSpline::knotPoints()
{
	return m_knot_points;
}

QVector<double> CubicSpline::accuracy()
{
	return m_roots_accuracy;
}

void CubicSpline::setPoints(const QList<QPointF> points)
{
	m_knot_points = points;
	m_interpolated = false;
	calculateSplineCoeffs();
}

void CubicSpline::calculateSplineCoeffs()
{
	if (m_knot_points.isEmpty()){
        qDebug()<<"Nothing to interpolate";
        return;
    }

    // number of points
	int n = m_knot_points.length();

	// cubic spline coefficients
	m_a.clear();
	m_b.clear();
	m_c.clear();
	m_d.clear();

	m_a.reserve(n);
	m_b.reserve(n);
	m_c.reserve(n);
	m_d.reserve(n);

    // Tridiagonal matrix algorithm's coefficients
    QVector<double> alpha(n);
    QVector<double> beta(n);
    // Diagonal elements of matrix A (Ax = F)
    QVector<double> A(n);
    QVector<double> B(n);
    QVector<double> C(n);
    // Right part of the matrix equation (Ax = F)
    QVector<double> F(n);
    // x(i+1) - x(i)
    QVector<double> h(n);
    // (f(i+1) - f(i))/h[i]
    QVector<double> delta(n);

	double x3 = m_knot_points[2].x() - m_knot_points[0].x();
	double xn = m_knot_points[n-1].x() - m_knot_points[n-3].x();

    // Fill variables
    for (int i = 0; i < n-1; i++){
		m_a.push_back(m_knot_points[i].y());
		h[i] = m_knot_points[i+1].x() - m_knot_points[i].x();
		delta[i] = (m_knot_points[i+1].y() - m_knot_points[i].y())/h[i];
        A[i] = h[i];
        B[i] = h[i];
        F[i] = (i > 0) ? 3*(h[i]*delta[i-1] + h[i-1]*delta[i]) : 0;
    }

    A[0] = x3;
    C[0] = h[0];

    for (int i = 1; i < n - 1; i++){
        C[i] = 2*(h[i] + h[i-1]);
    }

    C[n-1] = h[n-2];
    B[n-1] = xn;
    A[n-1] = h[n-2];

    int i = n - 1;
    F[0] = ((h[0] + 2*x3)*h[1]*delta[0] + pow(h[0], 2)*delta[1])/x3;
    F[n-1] = (pow(h[i-1],2)*delta[i-2] + (2*xn + h[i-1])*h[i-2]*delta[i-1])/xn;

    // Tridiagonal matrix algorithm
    alpha[0] = -B[0]/C[0];
    beta[0] = F[0]/C[0];

    for (i = 1; i < n - 1; i++){
        alpha[i] = -B[i]/(C[i] + A[i]*alpha[i-1]);
        beta[i] = (F[i] - A[i]*beta[i-1])/(C[i] + A[i]*alpha[i-1]);
    }

	m_b.push_front((F[n-1] - A[n-1]*beta[n-2])/(C[n-1] + A[n-1]*alpha[n-2]));

    // Filling cubic spline coefficients
	for (i = n-2; i >= 0; i--){
		m_b.push_front(m_b[0]*alpha[i] + beta[i]);
    }
    for (i = 0; i < n-1; i ++){
		m_d.push_back((m_b[i+1] + m_b[i] - 2*delta[i])/(h[i]*h[i]));
		m_c.push_back(2*(delta[i] - m_b[i])/h[i] - (m_b[i+1] - delta[i])/h[i]);
    }

	m_interpolated = true;
}

template<class T>
int CubicSpline::sgn(T value)
{
    return value < T(0)? -1: value > T(0);
}
