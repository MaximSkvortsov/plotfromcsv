#ifndef SINCAPPROXIMATOR_H
#define SINCAPPROXIMATOR_H

#include <QObject>
#include <QThread>
#include <QVector>
#include <QList>
#include <QPointF>
#include <functional>
#include <memory>

class SincApproximator : public QThread
{
	Q_OBJECT
	void run() Q_DECL_OVERRIDE;
private:
	class Simplex;
	std::unique_ptr<Simplex> m_simplex;
	QList<QPointF> m_result_points;
	double m_step;

public:
	// constructor's and destructor's initializations must be in .cpp because of
	// usage of unique_ptr for nested class (Simplex))
	SincApproximator(QObject *parent = nullptr);
	~SincApproximator();
	void setPoints(QList<QPointF> points);
	void setStep(const double step) {m_step = step;}
	QList<QPointF> getResult() {return m_result_points;}
	void clear();

signals:
	void resultReady();
};
#endif // SINCAPPROXIMATOR_H
