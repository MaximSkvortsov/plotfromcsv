#include "scrollspinbox.h"
#include <QWidget>
#include <QWheelEvent>

ScrollSpinBox::ScrollSpinBox(QWidget* widget):QSpinBox(widget){}

ScrollSpinBox::~ScrollSpinBox(){}

void ScrollSpinBox::wheelEvent(QWheelEvent *event){
    int value = this->value();
    if (event->angleDelta().y() > 0){
        this->setValue(value + 1);
    }
    else{
        this->setValue(value - 1);
    }
}
