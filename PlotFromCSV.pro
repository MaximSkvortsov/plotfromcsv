#-------------------------------------------------
#
# Project created by QtCreator 2016-11-10T12:14:56
#
#-------------------------------------------------

QT       += core gui
QT       += charts
QT       += printsupport
QT       += concurrent widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PlotFromCSV
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    scrollspinbox.cpp \
    cubicspline.cpp \
    draglabel.cpp \
    chartwidget/chartwidget.cpp \
    chartwidget/cw_mouse_handlers.cpp \
    chartwidget/contourline.cpp \
    chartwidget/csvhandler.cpp \
    chartwidget/qvalueaxisreserve.cpp \
    chartwidget/interpolationassistant.cpp \
    vsectionfield.cpp \
    sincapproximator.cpp


HEADERS  += mainwindow.h \
    scrollspinbox.h \
    cubicspline.h \
    draglabel.h \
    chartwidget/chartwidget.h \
    chartwidget/contourline.h \
    chartwidget/csvhandler.h \
    chartwidget/qvalueaxisreserve.h \
    chartwidget/interpolationassistant.h \
    vsectionfield.h \
    sincapproximator.h

TRANSLATIONS    = doc/plotfromcsv_ru.ts

FORMS    += mainwindow.ui

INCLUDEPATH += .\
INCLUDEPATH += ./chartwidget \

RESOURCES     = resources.qrc

DEFINES += KSI=\\\"$$KSI\\\"
