#include <QPropertyAnimation>

#include "vsectionfield.h"

VSectionField::VSectionField(const int animationDuration, QWidget* parent)
	: QWidget(parent), animationDuration(animationDuration)
{
	toggleButton = new QToolButton(this);
	headerLine = new QFrame(this);
	toggleAnimation = new QParallelAnimationGroup(this);
	contentArea = new QScrollArea(this);
	mainLayout = new QGridLayout(this);

	toggleButton->setStyleSheet("QToolButton {border: none;}");
	toggleButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	toggleButton->setArrowType(Qt::ArrowType::DownArrow);
	toggleButton->setCheckable(true);
	toggleButton->setChecked(false);

	headerLine->setFrameShape(QFrame::HLine);
	headerLine->setFrameShadow(QFrame::Sunken);
	headerLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	// start out collapsed
	contentArea->setMaximumWidth(0);
	contentArea->setMinimumWidth(0);	

	// let the entire widget grow and shrink with its content
	toggleAnimation->addAnimation(new QPropertyAnimation(this, "minimumWidth"));
	toggleAnimation->addAnimation(new QPropertyAnimation(this, "maximumWidth"));
	toggleAnimation->addAnimation(new QPropertyAnimation(contentArea, "maximumWidth"));

	mainLayout->setVerticalSpacing(0);
	mainLayout->setContentsMargins(0, 0, 0, 0);

	int row = 0;
	mainLayout->addWidget(toggleButton, row, 1, 1, 1);
	mainLayout->addWidget(headerLine, row++, 0, 1, 1);
	mainLayout->addWidget(contentArea, row, 0, 1, 2);
	setLayout(mainLayout);

	contentArea->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	connect(toggleButton, &QToolButton::clicked, [this](const bool checked)
	{
		toggleButton->setArrowType(checked ? Qt::ArrowType::LeftArrow : Qt::ArrowType::DownArrow);
		toggleAnimation->setDirection(checked ? QAbstractAnimation::Forward : QAbstractAnimation::Backward);
		toggleAnimation->start();
	});
}

VSectionField::VSectionField(QWidget *parent)
	: VSectionField(200, parent)
{
}

void VSectionField::setContentLayout(QLayout & contentLayout)
{
	delete contentArea->layout();
	contentArea->setLayout(&contentLayout);
	const auto collapsedWidth = sizeHint().width() - contentArea->maximumWidth();
	auto contentWidth = contentLayout.sizeHint().width();

	for (int i = 0; i < toggleAnimation->animationCount() - 1; ++i)
	{
		QPropertyAnimation* SectionAnimation = static_cast<QPropertyAnimation *>(toggleAnimation->animationAt(i));
		SectionAnimation->setDuration(animationDuration);
		SectionAnimation->setStartValue(collapsedWidth);
		SectionAnimation->setEndValue(collapsedWidth + contentWidth);
	}

	QPropertyAnimation* contentAnimation = static_cast<QPropertyAnimation *>(toggleAnimation->animationAt(toggleAnimation->animationCount() - 1));
	contentAnimation->setDuration(animationDuration);
	contentAnimation->setStartValue(0);
	contentAnimation->setEndValue(contentWidth);
}
