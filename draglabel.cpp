#include "draglabel.h"
#include <QDebug>
#include <QPainter>
#include <QMouseEvent>
#include <QFontMetrics>

DragLabel::DragLabel(QWidget* parent = Q_NULLPTR):QLabel(parent)
{
    hided_by_user = false;

    // set size policy as "Minimum"
    this->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);

    //create "Hide" action for context menu
    hide_label = new QAction("Hide", this);
    // connect action with function hide of QLabel
    connect(hide_label, &QAction::triggered, this, &DragLabel::hide);

    // create new menu(context menu)
    context_menu = new QMenu(this);
    // add action "Hide" to menu
    context_menu->addAction(hide_label);

    // set context menu policy as "CustomContextMenu"
    this->setContextMenuPolicy(Qt::CustomContextMenu);

    // connect custom context menu request to slot "customContextMenu"
	connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(customContextMenu()));
}

DragLabel::~DragLabel()
{
    delete hide_label;
    delete context_menu;
}

void DragLabel::setAxisCoords(double x, double y)
{
    if (x != NAN){
        axis_x_coord = x;
    }

    if (y != NAN){
        axis_y_coord = y;
    }
}

double DragLabel::getAxisX()
{
    return axis_x_coord;
}

double DragLabel::getAxisY()
{
    return axis_y_coord;
}

bool DragLabel::getHideReason()
{
    return hided_by_user;
}

void DragLabel::setHideReason(bool userHided)
{
    hided_by_user = userHided;
}


void DragLabel::hide(){
    QLabel::hide();

    hided_by_user = true;
}


// Left Button Click
// drag label
void DragLabel::mousePressEvent(QMouseEvent *event)
{
    // if left button click
    if (event->button() == Qt::LeftButton){
        // set drag flag "true" and remember position
        drag = true;
        drag_x = event->globalX();
        drag_y = event->globalY();

        // set cursor to "closed hand"
        this->setCursor(Qt::ClosedHandCursor);
    }
}

// Dragging label
void DragLabel::mouseMoveEvent(QMouseEvent *event)
{
	// if user holds left button
    if(drag){
		QRect newGeometry = geometry();

		// set new label geometry
		newGeometry.setX(geometry().x() + event->globalX() - drag_x);
		newGeometry.setY(geometry().y() + event->globalY() - drag_y);

		// set old width and heght
		// if you will change only x and y in geometry,
		// label will expand of shrink
		newGeometry.setWidth(geometry().width());
		newGeometry.setHeight(geometry().height());

		this->setGeometry(newGeometry);

		// remember coordinates
		drag_x = event->globalX();
		drag_y = event->globalY();
    }

}

// changes cursor to arrow and resets drag flag
void DragLabel::mouseReleaseEvent(QMouseEvent *event)
{
    drag = false;
    this->setCursor(Qt::ArrowCursor);
    event->accept();
}

void DragLabel::paintEvent(QPaintEvent *event)
{
	QFont font = this->font();
	QFontMetrics fontMetrics(font);
	QStringList textRows = this->text().split(QRegExp("[\n]+"), QString::SkipEmptyParts);
	int textMaxWidth = 0;
	foreach (auto str, textRows) {
		int tmp = fontMetrics.width(str);
		if (tmp > textMaxWidth)
			textMaxWidth = tmp;

	}
	int textHeight = fontMetrics.height();
	int labelHeight = 2*textHeight;
	this->setGeometry(this->pos().x(), this->pos().y(), textMaxWidth, labelHeight);
	this->setStyleSheet("QLabel { color : black; }");

	// drawing backgroung for label
	QPainter paint;
	// white semi transparent rectangle
	paint.begin (this);
	paint.setBrush (QBrush (QColor (255, 255, 255, 200)));
	paint.setPen (Qt::NoPen);
	paint.drawRect (0, 0, width(), height());
	paint.end();

    QLabel::paintEvent(event);
}

// custom context menu
void DragLabel::customContextMenu()
{
    // menu position
    QPoint menuPosition(cursor().pos().x(), cursor().pos().y());

    // popup menu
    context_menu->popup(menuPosition);
}
