#ifndef DRAGLABEL_H
#define DRAGLABEL_H

#include <cmath>
#include <QLabel>
#include <QMenu>
#include <QObject>

/* It's simple QLabel but with additional functionality - dragging label
 * User can drag label using LMB. Also it have flag - hided_by_user which
 * needed to show label when it was hidden because it is out of axis range.
 * QPaintEvent overrided with one addition - semi transparent white background.
 * Context menu provides one ability - hide label
*/
class DragLabel : public QLabel
{
    Q_OBJECT

public:
    DragLabel(QWidget* parent);
    ~DragLabel();
    void setAxisCoords(double x = NAN, double y = NAN);
    double getAxisX();
    double getAxisY();

    bool getHideReason();

    void setHideReason(bool userHided);

    void hide();

private:
    // drag flag
    bool drag;

	// needed to show label when it was hidden because its out of axis range
	bool hided_by_user;

    // variables to remember old coordinates when dragging
	int drag_x;
	int drag_y;

    // axis coords of label
	double axis_x_coord;
	double axis_y_coord;

    // context menu for label
	QMenu* context_menu;

    // context menu action
	QAction* hide_label;

protected:
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void paintEvent(QPaintEvent* event);

public slots:
    // handler for signal "customContextMenuRequested"
    void customContextMenu();


};

#endif // DRAGLABEL_H
