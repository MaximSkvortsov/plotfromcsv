#include "qvalueaxisreserve.h"

#include <float.h>

QValueAxisReserve::QValueAxisReserve(QObject *parent) : QValueAxis(parent)
{
	reserve = false;
	reserve_coeff = 0.1;
	reserve_value_max = 0;
	reserve_value_min = 0;
	reserve_min_limit = 0;
	reserve_max_limit = 1;
	min_limit = 0;
	max_limit = 1;
	default_limits = qMakePair(min_limit, max_limit);
	axisEditor = std::make_shared<QWidget>();
	custom = false;
	minLimitValidator = new QDoubleValidator(-DBL_MAX, DBL_MAX, 6);
	maxLimitValidator = new QDoubleValidator(-DBL_MAX, DBL_MAX, 6);
	tickCountValidator = new QIntValidator(2, 35, this);
	initEditor();
}
void QValueAxisReserve::setLimits(double new_min_limit, double new_max_limit)
{
	min_limit = new_min_limit;
	max_limit = new_max_limit;
	if (!custom)
		default_limits = qMakePair(min_limit, max_limit);
	calculateReserve();
}

void QValueAxisReserve::setReserveLimits(const double new_min_limit, const double new_max_limit)
{
	reserve_min_limit = new_min_limit;
	reserve_max_limit = new_max_limit;
	calculateReserve();
}

void QValueAxisReserve::setReserveMinLimit(const double new_min_limit)
{
	reserve_min_limit = new_min_limit;
	calculateReserve();
}

void QValueAxisReserve::setReserveMaxLimit(const double new_max_limit)
{
	reserve_max_limit = new_max_limit;
	calculateReserve();
}

void QValueAxisReserve::setReserveCoeff(double new_reserve_coeff)
{
	reserve_coeff = new_reserve_coeff;
	calculateReserve();
}

// limits for validator of min value in axisEditor
void QValueAxisReserve::setMinValueLimits(double min, double max)
{
	if (min >= max)
		return;
	minLimitValidator->setBottom(min);
	minLimitValidator->setTop(max);
}

// limits for validator of max value in axisEditor
void QValueAxisReserve::setMaxValueLimits(double min, double max)
{
	if (min >= max)
		return;
	maxLimitValidator->setBottom(min);
	maxLimitValidator->setTop(max);
}

// limit for validator of tick count value in axisEditor
void QValueAxisReserve::setTickCountLimit(uint limit)
{
	tickCountValidator->setTop(limit);
}

void QValueAxisReserve::enableReserve()
{
	reserve = true;
	calculateReserve();
}

void QValueAxisReserve::disableReserve()
{
	reserve = false;
	calculateReserve();
}

void QValueAxisReserve::resetToLimits()
{
	if (custom) {
		setLimits(default_limits.first, default_limits.second);
		custom = false;
	}
	calculateReserve();
}

double QValueAxisReserve::length()
{
	return max() - min();
}

double QValueAxisReserve::lengthMax()
{
	return max_limit - min_limit + reserve_value_max + reserve_value_min;
}

QPair<double,double> QValueAxisReserve::limits()
{
	return QPair<double,double>(min_limit, max_limit);
}

QPair<double, double> QValueAxisReserve::limitsWithReserve()
{
	return QPair<double,double>(min_limit - reserve_value_min, max_limit + reserve_value_max);
}

bool QValueAxisReserve::isReserveEnabled()
{
	return reserve;
}

bool QValueAxisReserve::isLimitOvercome()
{
	if (min() < min_limit - reserve_value_min)
		return true;
	if (max() > max_limit + reserve_value_max)
		return true;
	return false;
}

void QValueAxisReserve::calculateReserve()
{
	if (reserve && !custom){
		reserve_value_min = (max_limit - min_limit)*reserve_coeff;
		if (min_limit - reserve_value_min < reserve_min_limit && reserve_min_limit <= min_limit)
			reserve_value_min = min_limit - reserve_min_limit;
		reserve_value_max = (max_limit - min_limit)*reserve_coeff;
		if (max_limit + reserve_value_max > reserve_max_limit && reserve_max_limit >= max_limit)
			reserve_value_max = reserve_max_limit - max_limit;
		setRange(min_limit - reserve_value_min, max_limit + reserve_value_max);

	}
	if (!reserve || custom){
		reserve_value_min = 0;
		reserve_value_max = 0;
		setRange(min_limit, max_limit);
	}
}

void QValueAxisReserve::initEditor()
{
	QGridLayout * mainLayout = new QGridLayout(axisEditor.get());
	QLabel * minValueLabel = new QLabel(tr("Min:"), axisEditor.get());
	QLabel * maxValueLabel = new QLabel(tr("Max:"), axisEditor.get());
	QLabel * tickCountLabel = new QLabel(tr("Grid lines:"), axisEditor.get());
	QLineEdit * minValueLineEdit = new QLineEdit(axisEditor.get());
	QLineEdit * maxValueLineEdit = new QLineEdit(axisEditor.get());
	QLineEdit * tickCountLineEdit = new QLineEdit(axisEditor.get());
	QPushButton * resetButton = new QPushButton(tr("Reset"),axisEditor.get());

	minValueLineEdit->setValidator(minLimitValidator);
	maxValueLineEdit->setValidator(maxLimitValidator);
	tickCountLineEdit->setValidator(tickCountValidator);

	QPair<double,double> limits = limitsWithReserve();
	minValueLineEdit->setText(QString::number(limits.first));
	maxValueLineEdit->setText(QString::number(limits.second));
	tickCountLineEdit->setText(QString::number(this->tickCount()));

	mainLayout->addWidget(minValueLabel,0,1);
	mainLayout->addWidget(minValueLineEdit,0,2);
	mainLayout->addWidget(maxValueLabel,0,3);
	mainLayout->addWidget(maxValueLineEdit,0,4);
	mainLayout->addWidget(tickCountLabel,0,5);
	mainLayout->addWidget(tickCountLineEdit,0,6);
	mainLayout->addWidget(resetButton,0,7);

	tickCountLineEdit->resize(20, tickCountLineEdit->height());
	minValueLineEdit->resize(100, tickCountLineEdit->height());
	maxValueLineEdit->resize(100, tickCountLineEdit->height());
	tickCountLineEdit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	minValueLineEdit->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
	maxValueLineEdit->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);

	connect(minValueLineEdit, SIGNAL(textEdited(QString)), this, SLOT(setNewMin(QString)));
	connect(maxValueLineEdit, SIGNAL(textEdited(QString)), this, SLOT(setNewMax(QString)));
	connect(tickCountLineEdit, SIGNAL(textEdited(QString)), this, SLOT(setNewTickCount(QString)));
	connect(resetButton, SIGNAL(pressed()), this, SLOT(reset()));
	connect(this, &QValueAxisReserve::tickCountChanged, [tickCountLineEdit, this](int val)
	{
		QString value = QString::number(val, 'G', 15);
		if (tickCountLineEdit->text() == value)
			return;
		tickCountLineEdit->setText(value);
	});
	connect(this, &QValueAxisReserve::minChanged, [minValueLineEdit, this](int)
	{
		QPair<double,double> limits = limitsWithReserve();
		QString value = QString::number(limits.first, 'G', 15);
		if (minValueLineEdit->text() == value)
			return;
		minValueLineEdit->setText(value);
	});
	connect(this, &QValueAxisReserve::maxChanged, [maxValueLineEdit, this](int)
	{
		QPair<double,double> limits = limitsWithReserve();
		QString value = QString::number(limits.second, 'G', 15);
		if (maxValueLineEdit->text() == value)
			return;
		maxValueLineEdit->setText(value);
	});

	axisEditor->setLayout(mainLayout);
}

void QValueAxisReserve::setNewMin(QString value)
{
	double newMin = 0;
	try{
		newMin = value.toDouble();
	} catch(...) { return; }
	if (newMin > limitsWithReserve().second)
		return;

	if (!custom)
		default_limits = qMakePair(min_limit, max_limit);
	min_limit = newMin;
	custom = true;

	calculateReserve();
	emit userChangedValue();
}

void QValueAxisReserve::setNewMax(QString value)
{
	double newMax = 0;
	try{
		newMax = value.toDouble();
	} catch(...) { return; }
	if (newMax < limitsWithReserve().first)
		return;

	if (!custom)
		default_limits = qMakePair(min_limit, max_limit);
	max_limit = newMax;
	custom = true;

	calculateReserve();
	emit userChangedValue();
}

void QValueAxisReserve::setNewTickCount(QString value)
{
	int newTickCount = 0;
	try{
		newTickCount = value.toInt();
	} catch (...) {
		return;
	}

	setTickCount(newTickCount);
	custom = true;

	emit userChangedValue();
}

void QValueAxisReserve::reset()
{
	custom = false;
	min_limit = default_limits.first;
	max_limit = default_limits.second;
	emit resetTickCount();
	calculateReserve();
}
