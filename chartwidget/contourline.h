#ifndef CONTOURLINE_H
#define CONTOURLINE_H
#include <QWidget>
#include <QtCharts>

using namespace QtCharts;

class ContourLine
{
public:
	ContourLine(double x_min = 0, double x_max = 1, double value = 0);
	QLineSeries *series();

	void setBegin(double x)	{m_begin = x;}
	void setEnd(double x)	{m_end = x;}
	void setVisible(bool state)		{m_contour_line->setVisible(state);}
	void setValue(double y_value)	{m_value = y_value;}
    void setBeginEnd(double x_min, double x_max){setBegin(x_min); setEnd(x_max);}

	bool isVisible()	{return m_contour_line->isVisible();}
	bool isEmpty()		{return m_contour_line->points().isEmpty();}
	double value()	{return m_value;}
	void update(){m_contour_line = this->series();}

private:
	QLineSeries *m_contour_line;
	double m_value;
	double m_begin;
	double m_end;
};
#endif // CONTOURLINE_H
