#ifndef CHARTWIDGET_H
#define CHARTWIDGET_H
#include <QtCharts>
#include <memory>
#include "draglabel.h"
#include "cubicspline.h"
#include "contourline.h"
#include "csvhandler.h"
#include "qvalueaxisreserve.h"
#include "interpolationassistant.h"
#include "vsectionfield.h"

using namespace QtCharts;

class ChartWidget : public QChartView
{
	Q_OBJECT

public:
	ChartWidget(QChart* chart, QWidget* widget = Q_NULLPTR);
	ChartWidget(QWidget* parent);
	~ChartWidget(){}

	void loadData();
	void setCalculatorParam(const QString &key, double value);
	void setOpenPath(const QString &path);
	void setSavePath(QString path);
	void setFile(const QString &filename);
	void setBasis(const double &value);
	void setContourLine(const double &value);
	void setContourLineVisible(const bool state);
	void setInterpolatedDataVisible(const bool state);
	void setSectionVisible(const bool state);
	void setDecibelScale(const bool state);
	void setIntersectionWidthsVisible(const bool state);
	void setSincSeriesVisible(const bool state);

	void sectionByColumn(const int &value);
	void sectionByRow(const int &value);

	void print();
	void saveAs();

	void showAllIntersectionLabels();
	void hideAllIntersectionLabels();

	void saveSettings();
	void loadSettings();

	void sincApproximation(const double &step = 0.01);

	QList<QPair<int, double>> getMaxValues(bool row);

	QSize dataSize();
	QString currentFile();
	QString currentPath();
	double contourLineValue();

	QWidget *getAxesEditWidget();
	double getCalculatorParam(const QString &key);

signals:
	void sizeChanged();
	void sectionChanged();
	void replot();
	void fileChanged();
	void intersectionWidthsCalculationParamsChanged();
	void savePathChanged(QString path);
	void openPathChanged(QString path);
private slots:
	void replaceSincSeries(QList<QPointF> points, QPair<int,int> section = QPair<int,int>(-1, -1));
	void cursorOnGraph(QPointF coord, bool);
	void newSection();
private:
	void init();
	void createShortcuts();
	void csvSectionAsSeries();
	void newAxesLimits();
	void checkAxesOvercoming();
	void replotIntersectionLabels();
	void interpolateSectionSeries(const double &step = 0.01);
	void findIntersectionPoints();
	void findWidthBetweenIntersectionPoints(bool recalculate = false);
	void renderTo(QPaintDevice* paintDevice);
	void makeNiceAxis();
	QPointF findClosestPoint(const QPointF &point, QList<QPointF> points);

	bool select_area;
	bool drag;
	bool control;

	int drag_x;
	int drag_y;

	QRect selected_area;
	QRubberBand* selected_area_rubber_band;
	QPointF confineInPlotArea(double x, double y);

	std::shared_ptr<QValueAxisReserve> axis_x;
	std::shared_ptr<QValueAxisReserve> axis_y;

	std::shared_ptr<QLineSeries> section_series;
	std::shared_ptr<QLineSeries> interpolated_section_series;
	std::shared_ptr<QLineSeries> sincSeries;

	QList<std::shared_ptr<QLineSeries>> intersection_widths;
	QList<QPointF> intersection_points;
	QList<std::shared_ptr<DragLabel>> intersection_labels;

	std::unique_ptr<ContourLine> contour_line;
	std::unique_ptr<CSVHandler> csv_handler;
	std::unique_ptr<InterpolationAssistant> interpolationHelper;

	QString savePath;
	QString openPath;

	std::unique_ptr<QSettings> settings;

	std::unique_ptr<QTimer> replot_delay_timer;

	QMap<QString, double> intersectionWidthsCalculationParams = {
		{"ADC frequency", 1250000000},	// Частота АЦП [Hz]
		{"PFR", 2000},				// ЧПИ [Hz]
		{"speed", 50}
	};

protected:
	void mousePressEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void wheelEvent(QWheelEvent *event) override;
};

#endif // CHARTWIDGET_H
