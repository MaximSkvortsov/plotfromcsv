﻿#include <chartwidget/chartwidget.h>

// get current data size
QSize ChartWidget::getDataSize(){
	QSize temp;
	// if file was not uploaded, return 0,0
	if (!file_uploaded){
		temp.setHeight(0);
		temp.setWidth(0);
		return temp;
	}

	temp.setHeight(data.length());
	temp.setWidth(data[0].length());

	return temp;
}

// get curent file name
QString ChartWidget::getCurrentFile(){
	return current_file;
}

//get current path
QString ChartWidget::getCurrentPath(){
	return current_path;
}

// get current basis value
double ChartWidget::getBasis(){
	return basis;
}

// get current contour line value
double ChartWidget::getContourLine(){
	return contour_line->getValue();
}

// get current data size
QSize ChartWidget::getDataSize(){
	QSize temp;
	// if file was not uploaded, return 0,0
	if (!file_uploaded){
		temp.setHeight(0);
		temp.setWidth(0);
		return temp;
	}

	temp.setHeight(data.length());
	temp.setWidth(data[0].length());

	return temp;
}

// get curent file name
QString ChartWidget::getCurrentFile(){
	return current_file;
}

//get current path
QString ChartWidget::getCurrentPath(){
	return current_path;
}

// get current basis value
double ChartWidget::getBasis(){
	return basis;
}

// get current contour line value
double ChartWidget::getContourLine(){
	return contour_line;
}

// Fill sectionSeries with new data from currenFile
// mode == 0 - set series for row number "number"
// mode == 1 - set series for column number "number"
// on Y axis - values from file
// on X axis - number of element in row/column
void ChartWidget::setDataSection(const int number, unsigned int mode){

    // if file not uploaded or "number" < 0, cant set section
    if (!file_uploaded || number < 0){
        return;
    }

    // is "number" valid?
    if (mode){
        // if "number" > number of rows, do nothing
        if (number > data.length() - 1){
            return;
        }
    }
    else{
        // if "number" > number of columns, do nothing
        if (number > data[0].length() - 1){
            return;
        }
    }

    // if number valid and file uploaded, clear current section series
    section_series->clear();

	// clear current x and y axis limits
	x_limits.clear();
	y_limits.clear();

    // minimum and maximum for Y axis
	y_limits.append(INT_MAX);
	y_limits.append(INT_MIN);;

    // minimum and maximum for X axis
	x_limits.append(INT_MAX);
	x_limits.append(INT_MIN);

    double temp = 0;

    QVector<QPointF> points;

    switch(mode){
    // Set series for section by row number "number"
    case 0:{
        // add new points to section series
        for (int i = 0; i < data[0].length(); i++){

            // choose decibel scale or scale without changing data values
            if (decibels){
                temp = 20*log10(data[number][i]/basis);
            }
            else{
                temp = data[number][i];
            }
            //
            points.append(QPointF(i,temp));
            // find new minimum and maximum for Y axis
			y_limits[0] = qMin(y_limits[0], temp);
			y_limits[1] = qMax(y_limits[1], temp);
        }
        // set new minimum and maximum for X axis
		x_limits[0] = 0;
		x_limits[1] = data[0].length() - 1;

        // set new current row and set current column to default
        current_row = number;
        current_column = -1;

        break;
    }
        // Set series for section by column number "number"
    case 1:{
        // add new points to section series
        for (int i = 0; i < data.length(); i++){

            // choose decibel scale or scale without changing data values
            if (decibels){
                temp = 20*log10(data[i][number]/basis);
            }
            else{
                temp = data[i][number];
            }
            //
            points.append(QPointF(i,temp));
            // find new minimum and maximum for Y axis
			y_limits[0] = qMin(y_limits[0], temp);
			y_limits[1] = qMax(y_limits[1], temp);
        }
        // set new minimum and maximum for X axis
		x_limits[0] = 0;
		x_limits[1] = data.length() - 1;
        // set new current column and set current row to default
        current_column = number;
        current_row = -1;
        break;
    }
        // Mode is not 0 or 1
    default:{

        qDebug()<<"wrong mode";
        return;
    }
        //end of switch
    }

    section_series->replace(points);

	setYLimits(y_limits[0], y_limits[1]);

    // set new minimum and maximum for axis X
	axis_x->setMin(x_limits[0]);
	axis_x->setMax(x_limits[1]);

    // clear interpolated series because we changed section series
    interpolated_series->clear();
    // set flag currentSeriesInterpolated = false
    current_series_interpolated = false;

    // remove all intersection lines from series
    foreach(QLineSeries *series, intersection_widths){
        chart()->removeSeries(series);
    }
    // clear intersection lines
    intersection_widths.clear();

    // hide and clear intersection labels
    hideAllIntersectionLabels();
    intersection_labels.clear();

    makeNiceAxis();
}

// set valiable "currentPath" = "path"
void ChartWidget::setPath(const QString &path){
    current_path = path;
}

// set valiable "currentFile" = "filename"
// try to load data from new file using "currentPath" as path to file
void ChartWidget::setFile(const QString &filename){
    current_file = filename;
    loadData(current_path + current_file);
}

// set variable "basis" = "value" and recalculate data section series
void ChartWidget::setBasis(const double &value){
    // set basis = "value"
    basis = value;
    // set flag currentSeriesInterpolated = false
    current_series_interpolated = false;

    // recalculate section series according to current column and current row
    if(current_column == -1){
        setDataSection(current_row,0);
    }
    else{
        setDataSection(current_column,1);
    }
}

// set contour line according to value and recalculate intersection widths
void ChartWidget::setContourLine(const double &value){

    // if file not uploaded, do nothing
    if(!file_uploaded){
        return;
    }

	double contour_line_value = y_limits[0] + (y_limits[1] - y_limits[0])*value;

	contour_line->setBegin(section_series->points().first().x());
	contour_line->setEnd(section_series->points().last().x());
	contour_line->setValue(contour_line_value);

    hideAllIntersectionLabels();
    intersection_labels.clear();

    // recalculate intersection widths
    findWidthBetweenIntersectionPoints();
}

// show/hide contour line
// if "show" and contourLineSeries is empty, do nothing
void ChartWidget::setContourLineVisible(const bool state){
	contour_line->setVisible(state);
}

// show/hide interpolated series
// if "show" and interpolatedSeries is empty, calculate it
void ChartWidget::setInterpolatedDataVisible(const bool state){
    // if current series interpolated, show interpolated series and return
    if (current_series_interpolated){

        interpolated_series->setVisible(state);
        return;
    }

    // if state = "true" and current series is not interpolated, interpolate it and show
    if (state && file_uploaded && section_series->points().length() > 1){

        // interpolate section and fill unterpolated series with default step
        interpolateSectionSeries();

        current_series_interpolated = true;
        interpolated_series->setVisible(true);
    }
}

// show/hide section series
void ChartWidget::setSectionVisible(const bool state){
    section_series->setVisible(state);
}

// recalculate section data using decibel scale or using data values
void ChartWidget::setDecibelScale(const bool state)
{
    decibels = state;
    if (current_column == -1){
        setDataSection(current_row,0);
    }
    else{
        setDataSection(current_column,1);
    }
}

// show/hide intersection widths

void ChartWidget::setIntersectionWidthsVisible(const bool state)
{
    // if s and intersection widths is empty, calculate it
    if (intersection_widths.isEmpty()){
        if (state){
            findWidthBetweenIntersectionPoints();
        }
        else{
            return;
        }
    }

    // set all series visible or hide it
    foreach(QLineSeries* series, intersection_widths){
        series->setVisible(state);
    }
}


// sets new y-axis limits values
void ChartWidget::setYLimits(double new_y_min, double new_y_max)
{
	new_y_min = new_y_min - (new_y_max - new_y_min) * 0.1;
	new_y_max = new_y_max + (new_y_max - new_y_min) * 0.1;

	axis_y->setMin(new_y_min);
	axis_y->setMax(new_y_max);

	y_limits.clear();

	y_limits.push_back(new_y_min);
	y_limits.push_back(new_y_max);
}
