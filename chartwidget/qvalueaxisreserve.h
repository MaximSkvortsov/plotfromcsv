#ifndef QVALUEAXISRESERVE_H
#define QVALUEAXISRESERVE_H
#include <QtCharts>
#include <float.h>
#include <memory>

using namespace QtCharts;

class QValueAxisReserve : public QValueAxis
{
	Q_OBJECT
public:
	QValueAxisReserve(QObject *parent = NULL);
	~QValueAxisReserve(){}
	void setLimits(double new_min_limit, double new_max_limit);
	void setReserveLimits(const double new_min_limit, const double new_max_limit);
	void setReserveMinLimit(const double new_min_limit);
	void setReserveMaxLimit(const double new_max_limit);
	void setReserveCoeff(double new_reserve_coeff);
	void setMinValueLimits(double min = 0, double max = DBL_MAX);
	void setMaxValueLimits(double min = 0, double max = DBL_MAX);
	void setTickCountLimit(uint limit = 35);
	void enableReserve();
	void disableReserve();
	void resetToLimits();
	double length();
	double lengthMax();
	double reserveCoeff() {return reserve_coeff;}
	QPair<double,double> limits();
	QPair<double,double> limitsWithReserve();
	bool isReserveEnabled();
	bool isLimitOvercome();
	bool isCustom() {return custom;}
	QWidget* getEditor() {return axisEditor.get();}

private:
	void calculateReserve();
	void initEditor();
	bool reserve;
	bool custom;
	double min_limit;
	double max_limit;
	double reserve_coeff;
	double reserve_value_min;
	double reserve_value_max;
	double reserve_min_limit;
	double reserve_max_limit;
	QPair<double, double> default_limits;

	std::shared_ptr<QWidget> axisEditor;
	QDoubleValidator* minLimitValidator;
	QDoubleValidator* maxLimitValidator;
	QIntValidator* tickCountValidator;

private slots:
	void setNewMin(QString value);
	void setNewMax(QString value);
	void setNewTickCount(QString value);
	void reset();
signals:
	void userChangedValue();
	void resetTickCount();
};

#endif // QVALUEAXISRESERVE_H
