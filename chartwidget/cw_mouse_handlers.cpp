#include "chartwidget.h"

QPointF ChartWidget::confineInPlotArea(double x, double y)
{
	// plot area borders
	double top = chart()->plotArea().y();
	double bottom = top + chart()->plotArea().height();
	double left = chart()->plotArea().x();
	double right = left + chart()->plotArea().width();

	x = x < left ? left : x;
	x = x > right ? right : x;
	y = y < top ? top : y;
	y = y > bottom ? bottom : y;

	return QPointF(x,y);
}

void ChartWidget::mousePressEvent(QMouseEvent *event) {
	//Left button click && CTRL pressed. (Select area to zoom)
	if (event->modifiers() == Qt::CTRL && event->button() == Qt::LeftButton){
		QPointF event_pos = confineInPlotArea(event->x(),event->y());
		if (event_pos.x() != event->x() || event_pos.y() != event->y())
			return;
		// set selectedArea flag = "true" to handle it in mouse move event
		select_area = true;

		// set selected area geometry
		selected_area.setX(event->x());
		selected_area.setY(event->y());

		selected_area.setWidth(0);
		selected_area.setHeight(0);

		// set rubber band geometry and show it
		selected_area_rubber_band->setGeometry(selected_area);
		selected_area_rubber_band->show();

		return;
	}

	//Left button click. (Drag graph)
	if(event->button() == Qt::LeftButton){
		drag = true;
		drag_x = event->x();
		drag_y = event->y();

		this->setCursor(Qt::ClosedHandCursor);
		return;
	}

	//Right button click. (Reset)
	if(event->button() == Qt::RightButton){
		axis_x->resetToLimits();
		axis_y->resetToLimits();

		makeNiceAxis();
		replotIntersectionLabels();
		return;
	}
}

void ChartWidget::mouseReleaseEvent(QMouseEvent *event){

	//Area selected
	if(event->button() == Qt::LeftButton && select_area){
		// selected area geometry
		selected_area.setWidth(event->x() - selected_area.x());
		selected_area.setHeight(event->y() - selected_area.y());

		select_area = false;
		selected_area_rubber_band->hide();

		chart()->zoomIn(selected_area.normalized());
		checkAxesOvercoming();
		makeNiceAxis();
		replotIntersectionLabels();
	}

	//Drag released
	if(event->button() == Qt::LeftButton && drag){
		drag = false;
		this->setCursor(Qt::ArrowCursor);
	}
}

void ChartWidget::mouseMoveEvent(QMouseEvent *event){

	// Drag chart
	if ((event->buttons() & Qt::LeftButton) && drag && chart()->series().length() > 0){

		chart()->scroll(drag_x - event->x(), event->y() - drag_y);

		if (axis_x->isLimitOvercome())
			chart()->scroll(event->x() - drag_x, 0);
		if (axis_y->isLimitOvercome())
			chart()->scroll(0, drag_y - event->y());

		replotIntersectionLabels();

		drag_x = event->x();
		drag_y = event->y();

		return;
	}

	// Area select
	if((event->buttons() & Qt::LeftButton) && select_area){
		// new rubber band geometry
		QPointF event_pos = confineInPlotArea(event->x(), event->y());
		selected_area.setWidth(event_pos.x() - selected_area.x());
		selected_area.setHeight(event_pos.y() - selected_area.y());
		selected_area_rubber_band->setGeometry(selected_area.normalized());

		return;
	}

	// Cursor on graph
	{
		// get event pixel color
		QColor event_pixel_color = this->grab().toImage().pixelColor(event->pos());

		// find event pos relative (0.0)
		double x = event->x() - chart()->plotArea().x();
		double y = event->y() - chart()->plotArea().y();

		// if cursor near 0 sometimes x or y can be less that 0, those two lines fixes that
		x = x > 0 ? x : 0;
		y = y > 0 ? y : 0;

		// find event pos relative axis values
		x = axis_x->min() + x*axis_x->length()/chart()->plotArea().width();
		y = axis_y->max() - y*axis_y->length()/chart()->plotArea().height();

		this->setCursor(Qt::ArrowCursor);

		// cursor on section graph
		if (event_pixel_color == section_series->pen().color())
			emit section_series->hovered(QPointF(x, y),true);

		// cursor on contour line
		if (event_pixel_color == contour_line->series()->pen().color())
			emit contour_line->series()->hovered(QPointF(x, y),true);

		// cursor on interpolated graph
		if (event_pixel_color == interpolated_section_series->pen().color())
			emit interpolated_section_series->hovered(QPointF(x, y),true);

		// cursor on intersection widths
		if (intersection_widths.isEmpty())
			return;

		if(event_pixel_color == intersection_widths.first()->pen().color()) {
			foreach(auto series, intersection_widths) {
				if (!series->points().isEmpty()) {
					double first = series->points().first().x();
					double last = series->points().last().x();

					if (x >= first && x <= last)
						emit series->hovered(QPointF(x, y),true);
				}
			}
		}
	}
}


// if cursur on plot area, zoom to cursor
// if cursor on Y axis ot X axis, zoom only Y or X axis
void ChartWidget::wheelEvent(QWheelEvent *event){
	bool on_axis = false;
	QRectF zoom_rect = chart()->plotArea();
	QRectF plot_area = chart()->plotArea();

	// zoom coefficient
	double zoom_coeff = 1.10;
	// plot area borders
	double top = plot_area.y();
	double bottom = top + plot_area.height();
	double left = plot_area.x();
	double right = left + plot_area.width();

	// check wheelrotation direction
	zoom_coeff = event->angleDelta().y() > 0 ? 2 - zoom_coeff : zoom_coeff;

	// Mouse on X axis
	if (event->y() > bottom){
		zoom_rect.setX(zoom_rect.x() + zoom_rect.width()*(1-zoom_coeff));
		zoom_rect.setWidth(zoom_rect.width()*zoom_coeff);
		on_axis = true;
	}

	// Mouse on Y axis
	if (event->x() < left){
		zoom_rect.setY(zoom_rect.y() + zoom_rect.height()*(1 - zoom_coeff));
		zoom_rect.setHeight(zoom_rect.height()*zoom_coeff);
		on_axis = true;
	}

	// Mouse in plot area
	if (!on_axis && (event->x() < right || event->y() > top)){
		// calculace event pos relative to (0.0) of plot area
		double x = event->x() - plot_area.x();
		double y = event->y() - plot_area.y();

		// calculate new geometry
		double newX = plot_area.x() + x*(1 - zoom_coeff);
		double newY = plot_area.y() + y*(1 - zoom_coeff);
		double newWidth = x*zoom_coeff + (plot_area.width() - x)*zoom_coeff;
		double newHeight = y*zoom_coeff + (plot_area.height() - y)*zoom_coeff;

		// set new geometry
		zoom_rect.setX(newX);
		zoom_rect.setY(newY);
		zoom_rect.setWidth(newWidth);
		zoom_rect.setHeight(newHeight);
	}

	chart()->zoomIn(zoom_rect);
	checkAxesOvercoming();
	replotIntersectionLabels();
}
