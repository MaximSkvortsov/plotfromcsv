#include "contourline.h"

using namespace QtCharts;


ContourLine::ContourLine(double x_min, double x_max, double y_value)
{
	m_contour_line = new QLineSeries;
	m_begin = x_min;
	m_end = x_max;
	m_value = y_value;
	this->setVisible(false);
}

QLineSeries *ContourLine::series()
{
	double step = (m_end - m_begin)/50;
	QVector<QPointF> points;

	for (int i = 0; i*step + m_begin <= m_end; i++){
		points.push_back(QPointF(m_begin + i*step,m_value));
	}
	m_contour_line->replace(points);
	return m_contour_line;
}
