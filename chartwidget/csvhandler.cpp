#include "csvhandler.h"
#include <QFile>
#include <QObject>
#include <float.h>


CSVHandler::CSVHandler(QObject *parent) : QObject(parent)
{
	init();
}

CSVHandler::~CSVHandler()
{

}

// Loads data from file or if no filename received, opens file dialog.
int CSVHandler::openFile(QString file)
{
	// if filename not received, open file dialog
	if (file.isEmpty()) {
		QString filter = "*.csv";
		file = fileDialog->getOpenFileName(nullptr, "Choose .csv file", currentPath(), filter);
	}

	QFile data_file(file);
	if(!data_file.open(QIODevice::ReadOnly))
		return 1;

	// iterators
	int i = 0;
	int j = 0;

	data.clear();
	col_max_values.clear();
	row_max_values.clear();
	// read data from loaded file
	while (!data_file.atEnd()){
		// read line
		QString line = data_file.readLine();
		// split numbers
		QStringList temp = line.split(';');
		if (col_max_values.size() == 0)
			col_max_values.reserve(temp.length());
		QVector<double> temp_vec;
		temp_vec.clear();
		double row_max = -DBL_MAX;
		// add splited numbers to temporary vector
		for (j = 0; j < temp.length(); j++){
			double tmp = temp[j].toDouble();
			temp_vec.push_back(tmp);

			if(row_max < tmp)
				row_max = tmp;
			if (i == 0)
				col_max_values.append(qMakePair(j, tmp));
			if (i > 0 && col_max_values[j].second < tmp)
				col_max_values[j] = qMakePair(j, tmp);
		}
		// push data from temporary vector to object data vector
		data.push_back(temp_vec);
		i++;
		row_max_values.append(qMakePair(i, row_max));
	}
	// close file after all data read
	data_file.close();

	qSort(row_max_values.begin(), row_max_values.end(), [](const QPair<int,double> a, const QPair<int,double> b) { return (a.second > b.second); });
	qSort(col_max_values.begin(), col_max_values.end(), [](const QPair<int,double> a, const QPair<int,double> b) { return (a.second > b.second); });

	//set new current file
	m_filename = QFileInfo(file).fileName();
	emit fileChanged(m_filename);
	// set new current path
	m_path = QFileInfo(file).absolutePath();
	emit pathChanged(m_path);

	return 0;
}

// set new filename and try to load it from current path
void CSVHandler::setFile(QString new_filename)
{
	if (new_filename != "") {
		m_filename = new_filename;
		openFile(m_path + '/' +m_filename);
	}
}

void CSVHandler::setPath(QString new_path)
{
	m_path = new_path;
}

void CSVHandler::setBasis(const double &new_basis)
{
	basis = new_basis;
	if (decibel)
		sectionByCurrent();
}

void CSVHandler::decibelScale(bool state)
{
	decibel = state;
	sectionByCurrent();
}

QSize CSVHandler::dataSize()
{
	if (data.isEmpty())
		return QSize(0,0);

	int width = data[0].size();
	int height = data.size();

	return QSize(width, height);
}

QList<QPair<int, double> > CSVHandler::getMaxValues(bool row)
{
	if (row)
		return row_max_values;
	return col_max_values;
}

void CSVHandler::init()
{
	decibel = false;
	basis = 1;
	m_path = "";
	m_filename = "";
	current_row_col = QPair<int,int>(-1,-1);
	current_section.clear();
	fileDialog = new QFileDialog(0, "Choose .csv file", m_path, "*.csv");
}

// Creates section of current file by row or column
void CSVHandler::sectionBy(const int &number, bool mode)
{
	if (m_filename.isEmpty() || m_path.isEmpty())
		return;
	QSize size = dataSize();

	// is "number" valid?
	if (mode && number > size.width() - 1)
		return;
	if (!mode && number > size.height()- 1)
		return;


	// if number valid and file uploaded, clear current section series
	current_section.clear();
	double temp = 0;

	if (mode) {
		// Set series for section by column number "number"
		// add new points to section series
		for (int i = 0; i < size.height(); i++){

			// choose decibel scale or scale without changing data values
			if (decibel){
				temp = 20*log10(data[i][number]/basis);
			}
			else{
				temp = data[i][number];
			}
			//
			current_section.append(QPointF(i,temp));
		}
		// set new current column and set current row to default
		current_row_col.second = number;
		current_row_col.first = -1;
	} else {
		// Set series for section by row number "number"
		// add new points to section series
		for (int i = 0; i < size.width(); i++){

			// choose decibel scale or scale without changing data values
			if (decibel){
				temp = 20*log10(data[number][i]/basis);
			}
			else{
				temp = data[number][i];
			}
			//
			current_section.append(QPointF(i,temp));
		}
		// set new current column and set current row to default
		current_row_col.first = number;
		current_row_col.second = -1;
	}
}

// generates section of current file by current row/column
void CSVHandler::sectionByCurrent()
{
	if ( current_row_col.first != -1){
		sectionByRow(current_row_col.first);
	}
	if (current_row_col.second != -1){
		sectionByColumn(current_row_col.second);
	}
}
