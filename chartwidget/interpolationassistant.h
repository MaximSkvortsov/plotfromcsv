#ifndef INTERPOLATOR_H
#define INTERPOLATOR_H
#include <QtCharts>
#include <memory>
#include "cubicspline.h"

using namespace QtCharts;

class InterpolationAssistant
{
public:
	InterpolationAssistant();
	~InterpolationAssistant(){}
	QList<QPointF> interpolate(const QList<QPointF> knot_points, double step);
	QList<QPointF> findZeros(double value = 0);
	QList<std::shared_ptr<QLineSeries>> intersectionSeries(QPen pen);
	int segmentOf(double x, const QList<QPointF> &points);

private:
	std::unique_ptr<CubicSpline> spline;
	QList<QPointF> zeros;
};

#endif // INTERPOLATOR_H
