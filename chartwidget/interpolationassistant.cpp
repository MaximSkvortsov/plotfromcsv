#include "interpolationassistant.h"

InterpolationAssistant::InterpolationAssistant()
{
	spline = std::unique_ptr<CubicSpline>(new CubicSpline());
}

QList<QPointF> InterpolationAssistant::interpolate(const QList<QPointF> knot_points, double step)
{
	spline->setPoints(knot_points);
	return spline->interpolate(knot_points.first().x(), knot_points.last().x(), step);
}

QList<QPointF> InterpolationAssistant::findZeros(double value)
{
	double left_y = 0;
	double right_y = 0;
	// number of segments
	int n = spline->knotPoints().length() - 1;
	// clear current intersection points list
	zeros.clear();
	QList<QPointF> points = spline->knotPoints();
	// find intersection points
	for (int i = 0; i < n; i++){

		// if current y == value coincides, append current x and continue
        if (points[i].y() == value){
            zeros.append(points[i]);
			continue;
		}

        left_y = points[i].y();
        right_y = points[i+1].y();

		// if contour line dont intersect segment, continue
		if ((left_y <= value && right_y >= value) || (left_y >= value && right_y <= value))
			zeros.append(spline->solveAtSegment(value, i));
	}
	return zeros;
}

QList<std::shared_ptr<QLineSeries> > InterpolationAssistant::intersectionSeries(QPen pen)
{
	QList<std::shared_ptr<QLineSeries>> intersections;
	int n = zeros.length() - 1;

	for (int i = 0; i < n; i++){
		double dx = (zeros[i+1].x() - zeros[i].x())/50;
		double left_value = spline->at(zeros[i].x() + dx).y() - spline->at(zeros[i].x()).y();
		double right_value = spline->at(zeros[i+1].x() - dx).y() - spline->at(zeros[i+1].x()).y();
		if (left_value > 0 && right_value > 0){
			intersections.append(std::make_shared<QLineSeries>());
			intersections.last()->append(zeros[i]);
			intersections.last()->append(zeros[i+1]);
			intersections.last()->setPen(pen);
		}
	}

	return intersections;
}

int InterpolationAssistant::segmentOf(double x, const QList<QPointF> &points)
{
	CubicSpline cs(points, false);
	return cs.segmentOf(x);
}
