#include "chartwidget.h"

#include <QtCharts>
#include <QtMath>
#include <QPrinter>
#include <QPrintDialog>
#include <QStandardPaths>

#include "sincapproximator.h"

const int c = 299792458; // speed of light

using namespace QtCharts;

ChartWidget::ChartWidget(QChart* chart, QWidget* widget):QChartView(chart, widget){
	init();
}

ChartWidget::ChartWidget(QWidget *parent = nullptr):QChartView(parent){
	init();
}

// initialize object
void ChartWidget::init(){
	setMouseTracking(true);
	setRenderHint(QPainter::Antialiasing, true);

	select_area = false;
	drag = false;

	interpolationHelper = std::unique_ptr<InterpolationAssistant>(new InterpolationAssistant());
	contour_line = std::unique_ptr<ContourLine>(new ContourLine());
	csv_handler = std::unique_ptr<CSVHandler>(new CSVHandler());
	csv_handler->setPath(qApp->applicationDirPath());

	axis_x = std::make_shared<QValueAxisReserve>();
	axis_y = std::make_shared<QValueAxisReserve>();

	axis_y->setReserveCoeff(0.1);
	axis_y->enableReserve();
	axis_y->setReserveMinLimit(0);

	section_series = std::make_shared<QLineSeries>();
	interpolated_section_series = std::make_shared<QLineSeries>();
	sincSeries = std::make_shared<QLineSeries>();

	selected_area_rubber_band = new QRubberBand(QRubberBand::Rectangle,this);

	axis_x->setTitleText(tr("Units"));
	axis_y->setTitleText(tr("Amplitude"));

	this->chart()->setAxisX(axis_x.get());
	this->chart()->setAxisY(axis_y.get());

	this->chart()->legend()->setVisible(false);

	this->chart()->addSeries(section_series.get());
	this->chart()->addSeries(contour_line->series());
	this->chart()->addSeries(interpolated_section_series.get());
	this->chart()->addSeries(sincSeries.get());

	section_series->attachAxis(axis_x.get());
	section_series->attachAxis(axis_y.get());
	contour_line->series()->attachAxis(axis_x.get());
	contour_line->series()->attachAxis(axis_y.get());
	interpolated_section_series->attachAxis(axis_x.get());
	interpolated_section_series->attachAxis(axis_y.get());
	interpolated_section_series->setVisible(false);
	sincSeries->attachAxis(axis_x.get());
	sincSeries->attachAxis(axis_y.get());
	sincSeries->setVisible(true);

	settings = std::unique_ptr<QSettings>(new QSettings(QSettings::IniFormat, QSettings::UserScope, "zptel", "PlotFromCSV"));
	loadSettings();

	// timer to scroll fast (to delay calculation of all switched on functions when scrolling)
	replot_delay_timer = std::unique_ptr<QTimer>(new QTimer());
	replot_delay_timer->setInterval(400);
	replot_delay_timer->setSingleShot(true);

	connect(replot_delay_timer.get(), &QTimer::timeout, this, &ChartWidget::replot);

	connect(this, &ChartWidget::intersectionWidthsCalculationParamsChanged,
			this, [this]{this->findWidthBetweenIntersectionPoints(true);});
	connect(this, &ChartWidget::savePathChanged, &ChartWidget::setSavePath);
	connect(this, &ChartWidget::openPathChanged, &ChartWidget::setOpenPath);
	connect(csv_handler.get(), &CSVHandler::pathChanged, this, &ChartWidget::setOpenPath);

	connect(this, &ChartWidget::sizeChanged, &ChartWidget::replotIntersectionLabels);
	connect(this, &ChartWidget::sectionChanged, &ChartWidget::newSection);

	connect(section_series.get(), SIGNAL(hovered(QPointF,bool)),
			this, SLOT(cursorOnGraph(QPointF,bool)));
	connect(contour_line->series(), SIGNAL(hovered(QPointF,bool)),
			this, SLOT(cursorOnGraph(QPointF,bool)));
	connect(interpolated_section_series.get(), SIGNAL(hovered(QPointF,bool)),
			this, SLOT(cursorOnGraph(QPointF,bool)));
	connect(this, &ChartWidget::sizeChanged, &ChartWidget::makeNiceAxis);
	connect(axis_x.get(), &QValueAxisReserve::userChangedValue, [this](){chart()->update();});
	connect(axis_y.get(), &QValueAxisReserve::userChangedValue, [this](){chart()->update();});
	connect(axis_x.get(), &QValueAxisReserve::resetTickCount, this, &ChartWidget::makeNiceAxis);
	connect(axis_y.get(), &QValueAxisReserve::resetTickCount, this, &ChartWidget::makeNiceAxis);
	createShortcuts();
}

void ChartWidget::createShortcuts()
{
	QKeySequence save_keys(Qt::CTRL + Qt::Key_S);
	QKeySequence open_keys(Qt::CTRL + Qt::Key_O);
	QKeySequence print_keys(Qt::CTRL + Qt::Key_P);

	QShortcut *save = new QShortcut(save_keys,this);
	QShortcut *open = new QShortcut(open_keys, this);
	QShortcut *print = new QShortcut(print_keys, this);

	connect(save, &QShortcut::activated, this, &ChartWidget::saveAs);
	connect(open, &QShortcut::activated, this, &ChartWidget::loadData);
	connect(print, &QShortcut::activated, this, &ChartWidget::print);
}

// replace section_series points by section from csv_handler and make axis looks nice
void ChartWidget::csvSectionAsSeries()
{
	section_series->replace(csv_handler->currentSection());
	if (section_series->points().length() != 0){
		double y_min = INT_MAX;
		double y_max = INT_MIN;
		foreach(QPointF point, section_series->points()){
			y_min = y_min > point.y() ? point.y() : y_min;
			y_max = y_max < point.y() ? point.y() : y_max;
		}
		newAxesLimits();
	}
	makeNiceAxis();
	emit sectionChanged();
}

// set new axis limits
void ChartWidget::newAxesLimits()
{
	double y_min = INT_MAX;
	double y_max = INT_MIN;
	double x_min = section_series->points().first().x();
	double x_max = section_series->points().last().x();
	foreach(QPointF point, section_series->points()){
		y_min = y_min > point.y() ? point.y() : y_min;
		y_max = y_max < point.y() ? point.y() : y_max;
	}

	axis_y->setLimits(y_min, y_max);
	axis_x->setLimits(x_min, x_max);
}

// checking that user try to drag or zoom beyond axes limits
void ChartWidget::checkAxesOvercoming()
{
	if (!section_series->points().isEmpty()){
		QPair<double, double> y_min_max;
		QPair<double, double> x_min_max;

		if (axis_x->isReserveEnabled())
			x_min_max = axis_x->limitsWithReserve();
		else
			x_min_max = axis_x->limits();

		axis_x->setMin(qMax(x_min_max.first, axis_x->min()));
		axis_x->setMax(qMin(x_min_max.second, axis_x->max()));

		if (axis_y->isReserveEnabled())
			y_min_max = axis_y->limitsWithReserve();
		else
			y_min_max = axis_y->limits();

		axis_y->setMin(qMax(y_min_max.first, axis_y->min()));
		axis_y->setMax(qMin(y_min_max.second, axis_y->max()));
	}
}

// try to load data from file "file"
void ChartWidget::loadData()
{
	csv_handler->setPath(openPath);
	if (csv_handler->openFile())
		return;
	csvSectionAsSeries();
	// set new title for chart
	QSize array_size = csv_handler->dataSize();
	QString array_size_string = QString::number(array_size.height()) + "x" +
								QString::number(array_size.width()) + tr("  array");

	// set new chart title = file name + array size
	chart()->setTitle(csv_handler->currentFile() + " " + array_size_string);
	emit fileChanged();
}

void ChartWidget::setCalculatorParam(const QString &key, double value)
{
	intersectionWidthsCalculationParams[key] = value;
}


// replots intersection widths labels according to its axis coordinates
void ChartWidget::replotIntersectionLabels()
{
	foreach(auto label, intersection_labels){

		// if label is visible or was hidden by function
		if (label->isVisible() || !label->getHideReason()){

			double labelX = label->getAxisX();
			double labelY = label->getAxisY();

			// if label in current area
			if (labelX >= axis_x->min() && labelX <= axis_x->max()){
				if (labelY >= axis_y->min() && labelY <= axis_y->max()){

					// calculate axis step per pixel
					double axisYstep = axis_y->length() / chart()->plotArea().height();
					double axisXstep = axis_x->length() / chart()->plotArea().width();

					// calculate new position for label
					int newCoordX = chart()->plotArea().x() + (labelX - axis_x->min()) / axisXstep;
					int newCoordY = chart()->plotArea().y() + (axis_y->max() - labelY) / axisYstep;

					// move label to new position
					label->move(QPoint(newCoordX, newCoordY));
					label->show();
				}
				else{
					// if label not in current area, hide it
					label->hide();
					label->setHideReason(false);
				}
			}
			else{
				// if label not in current area, hide it
				label->hide();
				label->setHideReason(false);
			}
		}
	}
}

void ChartWidget::showAllIntersectionLabels()
{
	foreach(auto label, intersection_labels)
		label->show();

	replotIntersectionLabels();
}

void ChartWidget::hideAllIntersectionLabels()
{
	foreach(auto label, intersection_labels){
		label->hide();
		label->setHideReason(true);
	}
}

// makes axis looks nice
void ChartWidget::makeNiceAxis()
{
	if (axis_x->isCustom() && axis_y->isCustom())
		return;

	QSizeF chartSize = chart()->size();
	int grid_count_x = 10;
	for ( ; chartSize.width()/grid_count_x > 65; grid_count_x++) {}
	for ( ; chartSize.width()/grid_count_x < 35; grid_count_x--) {}

	if (!axis_x->isCustom())
		axis_x->setTickCount(grid_count_x);

	int grid_count_y = 10;
	for ( ; chartSize.height()/grid_count_y > 65; grid_count_y++) {}
	for ( ; chartSize.height()/grid_count_y < 35; grid_count_y--) {}

	if (!axis_y->isCustom())
		axis_y->setTickCount(grid_count_y);

}

void ChartWidget::sincApproximation(const double &step)
{
	if (section_series->points().length() == 0)
		return;
	QPair<int, int> section = csv_handler->currentRowCol();
	std::shared_ptr<SincApproximator> sincApproximator = std::make_shared<SincApproximator>();
	connect(sincApproximator.get(), &QThread::started, [](){QApplication::setOverrideCursor(Qt::BusyCursor);});
	connect(sincApproximator.get(), &QThread::finished, [](){QApplication::restoreOverrideCursor();});
	connect(sincApproximator.get(),
			&SincApproximator::resultReady,
			[this, sincApproximator, section]()
	{
		this->replaceSincSeries(sincApproximator->getResult(), section);
	} );

	sincApproximator->clear();
	sincApproximator->setStep(step);
	sincApproximator->setPoints(section_series->points());
	sincApproximator->start();
}

void ChartWidget::saveSettings()
{
	settings->setValue("OpenFileDirectory", QVariant(openPath));
	settings->setValue("SaveFileDirectory", QVariant(savePath));
}

void ChartWidget::loadSettings()
{
	openPath = settings->value("OpenFileDirectory", "").toString();
	savePath = settings->value("SaveFileDirectory", "").toString();
}

QList<QPair<int, double> > ChartWidget::getMaxValues(bool row)
{
	return csv_handler->getMaxValues(row);
}

// renders widget with all labels to "paintDevidce"
void ChartWidget::renderTo(QPaintDevice *paintDevice)
{
	// create new painter to paint on printer
	QPainter *painter = new QPainter;
	// initialize paiter with printer
	painter->begin(paintDevice);

	// set painter render hint equal to chart render hint
	if (this->renderHints().testFlag(QPainter::Antialiasing)){
		painter->setRenderHint(QPainter::Antialiasing);
	}
	// render chart to painter
	this->render(painter);

	foreach(auto label, intersection_labels){
		label->render(painter, label->pos());
	}

	// end painter. now chart will be printed
	painter->end();
	//free memory
	delete painter;
}

// open QPrintDialog to print graph
void ChartWidget::print(){

	// create new printer to print on it
	QPrinter *printer = new QPrinter;
	// create new print dialog to get printer to print
	QPrintDialog *printDialog = new QPrintDialog(printer);

	// set new title for print dialog
	printDialog->setWindowTitle(tr("Print graph"));

	// execute print dialog (wait until it closed)
	if(printDialog->exec() == QDialog::Accepted){
		// if QDialog::Accepted, set our printer to selected printer in print dialog
		printer = printDialog->printer();

		renderTo(printer);
	}
	else{
		// print dialog was closed. do nothing
		return;
	}

	// free memory
	delete printer;
	delete printDialog;
}


// open QFileDialog to choose file name to save
// render graph to image and try to save it
void ChartWidget::saveAs(){

	// set default path and file name
	QString filePath = savePath + "/untitled.png";

	// open file dialog to let useg choose name, suffix and path of file
	QString fileName = QFileDialog::getSaveFileName(this, tr("File name"), filePath, tr("Images (*.bmp *.png *.jpg *.jpeg *.ppm *.xbm *.xpm)"));

	// if file dialog was closed, do nothing
	if (fileName.isEmpty()){
		return;
	}
	emit savePathChanged(QFileInfo(fileName).absolutePath());
	// create image to draw chart on it and save
	QImage chartGraph(chart()->size().toSize(), QImage::Format_ARGB32_Premultiplied);

	// fill image with white
	chartGraph.fill(qRgba(0,0,0,0));

	renderTo(&chartGraph);

	// save image
	chartGraph.save(fileName,Q_NULLPTR,100);
}

// set section series as data section by column = "value"
void ChartWidget::sectionByColumn(const int &value){
	csv_handler->sectionByColumn(value);
	csvSectionAsSeries();
}


// set section series as data section by row = "value"
void ChartWidget::sectionByRow(const int &value){
	csv_handler->sectionByRow(value);
	csvSectionAsSeries();
}

// cubic spline interpolation of current section series
// also fills interpolated series with default step
void ChartWidget::interpolateSectionSeries(const double &step){
	// Если уже проинтерполировано, ничего не делать.
	if (!interpolated_section_series->points().isEmpty())
		return;

	QList<QPointF> interpolated_points = interpolationHelper->interpolate(section_series->points(), step);
	interpolated_section_series->replace(interpolated_points);
	QPair<double,double> y_min_max = axis_y->limits();

	double y_min = y_min_max.first;
	double y_max = y_min_max.second;
	foreach(QPointF point, interpolated_section_series->points()){
		y_min = y_min < point.y() ? y_min : point.y();
		y_max = y_max > point.y() ? y_max : point.y();
	}

	if (axis_y->limitsWithReserve().first < y_min && axis_y->limitsWithReserve().second > y_max)
		return;

	double top_difference = y_max - y_min_max.second;
	double bot_difference = y_min - y_min_max.first;

	double top_res_coeff = axis_y->reserveCoeff() +
						   top_difference*axis_y->reserveCoeff()/(y_min_max.second - y_min_max.first);
	double bot_res_coeff =  axis_y->reserveCoeff() +
							bot_difference*axis_y->reserveCoeff()/(y_min_max.second - y_min_max.first);

	axis_y->setReserveCoeff(qMax(top_res_coeff, bot_res_coeff));
}

// finds intersection points of interpolated graph and contour line
void ChartWidget::findIntersectionPoints(){

	// if current section not interpolated, interpolate it
	interpolateSectionSeries();
	intersection_points.clear();
	intersection_points = interpolationHelper->findZeros(contour_line->value());
}

// finds pairs of intersection points which are between graph
// guaranteed that all points between those two are above
void ChartWidget::findWidthBetweenIntersectionPoints(bool recalculate)
{
	// if file not uploaded or section is not interpolated, do nothing
	if (section_series->points().isEmpty())
		return;
	if (recalculate) {
		for (int i = 0; i < intersection_widths.size(); i++) {
			std::shared_ptr<QLineSeries> series = intersection_widths[i];
			double width = series->points().last().x() - series->points().first().x();
			double widthMeters = 0;
			if (csv_handler->getCurrentRowNubmer() >= 0) {
				widthMeters = width*c /  (intersectionWidthsCalculationParams["ADC frequency"]) / 2;
			} else {
				widthMeters = width*intersectionWidthsCalculationParams["speed"] / (intersectionWidthsCalculationParams["PFR"]);
			}
			intersection_labels[i]->setText(QString::number(width,'g', 3) + "\n" +
												QString::number(widthMeters,'g', 3));
		}
		return;
	}
	// find intersection points
	findIntersectionPoints();

	// new QPen to dwar all intersection widths with one color
	QPen pen;
	// solid lines with width = 3 and color = red
	pen.setBrush(Qt::SolidPattern);
	pen.setWidth(3);
	pen.setBrush(Qt::red);

	// remove current intersection widths from chart and set state
	foreach(auto series, intersection_widths)
		chart()->removeSeries(series.get());
	bool state = false;
	if (!intersection_widths.isEmpty())
		state = intersection_widths.first()->isVisible();

	// clear current intersection widths and labels
	intersection_widths.clear();
	intersection_labels.clear();

	intersection_widths = interpolationHelper->intersectionSeries(pen);

	foreach (auto series, intersection_widths){
		chart()->addSeries(series.get());
		series->setVisible(state);
		series->attachAxis(axis_x.get());
		series->attachAxis(axis_y.get());
		connect(series.get(), &QLineSeries::hovered, this, &ChartWidget::cursorOnGraph);

		double width = series->points().last().x() - series->points().first().x();
		double widthMeters = 0;
		if (csv_handler->getCurrentRowNubmer() >= 0) {
			widthMeters = width*c /  (intersectionWidthsCalculationParams["ADC frequency"]) / 2;
		} else {
			widthMeters = width*intersectionWidthsCalculationParams["speed"] / (intersectionWidthsCalculationParams["PFR"]);
		}

		intersection_labels.append(std::make_shared<DragLabel>(this));
		intersection_labels.last()->setText(QString::number(width,'g', 3) + "\n" +
											QString::number(widthMeters,'g', 3));
		intersection_labels.last()->setAxisCoords(width/2 + series->points().first().x(), series->points().first().y());
		intersection_labels.last()->hide();
	}
}

#include "chartwidget.h"

QSize ChartWidget::dataSize(){
	return csv_handler->dataSize();
}

QString ChartWidget::currentFile(){
	return csv_handler->currentFile();
}

QString ChartWidget::currentPath(){
	return csv_handler->currentPath();
}

double ChartWidget::contourLineValue(){
	return contour_line->value();
}

QWidget *ChartWidget::getAxesEditWidget()
{
	QWidget * widget = new QWidget();
	QVBoxLayout * mainLayout = new QVBoxLayout();

	QLabel * axisXLabel = new QLabel(tr("X axis parameters:"));
	QLabel * axisYLabel = new QLabel(tr("Y axis parameters:"));
	axisXLabel->setAlignment(Qt::AlignLeft);
	axisYLabel->setAlignment(Qt::AlignLeft);

	mainLayout->addWidget(axisXLabel);
	mainLayout->addWidget(axis_x->getEditor());
	mainLayout->addWidget(axisYLabel);
	mainLayout->addWidget(axis_y->getEditor());

	widget->setLayout(mainLayout);

	return widget;
}

double ChartWidget::getCalculatorParam(const QString &key)
{
	return intersectionWidthsCalculationParams[key];
}

void ChartWidget::replaceSincSeries(QList<QPointF> points, QPair<int,int> section)
{
	if (section == csv_handler->currentRowCol() || section == QPair<int, int>(-1, -1))
		sincSeries->replace(points);
}

void ChartWidget::cursorOnGraph(QPointF coord, bool)
{
	this->setCursor(Qt::CrossCursor);

	QPointF temp;
	QObject *obj = sender();
	if (obj == section_series.get())
		temp = findClosestPoint(coord, section_series->points());
	if (obj == interpolated_section_series.get())
		temp = findClosestPoint(coord, interpolated_section_series->points());
	if (obj == contour_line->series())
		temp = findClosestPoint(coord, contour_line->series()->points());
	if (!temp.isNull()){
		QString point = "(" + QString::number(temp.x()) + "," + QString::number(temp.y()) + ")";
		QToolTip::showText(cursor().pos(),point,this);
	}
	int counter = 0;
	foreach(auto series, intersection_widths){
		if (obj == series.get() && !intersection_labels[counter]->isVisible()){
			intersection_labels[counter]->move(cursor().pos() - mapToGlobal(QPoint(0,0)));
			intersection_labels[counter]->show();
		}
		counter++;
	}
}

void ChartWidget::newSection()
{
	interpolated_section_series->hide();
	interpolated_section_series->clear();
	hideAllIntersectionLabels();
	intersection_labels.clear();
	foreach(auto series, intersection_widths)
		series->hide();
	intersection_widths.clear();
	sincSeries->hide();
	sincSeries->clear();

	replot_delay_timer->start();
}

void ChartWidget::setOpenPath(const QString &path){
	openPath = path;
	csv_handler->setPath(path);
	settings->setValue("OpenFileDirectory", QVariant(openPath));
}

void ChartWidget::setSavePath(QString path)
{
	savePath = path;
	settings->setValue("SaveFileDirectory", QVariant(savePath));
}

void ChartWidget::setFile(const QString &filename){
	csv_handler->setFile(filename);
}

void ChartWidget::setBasis(const double &value){
	csv_handler->setBasis(value);
	csvSectionAsSeries();
}

void ChartWidget::setContourLine(const double &value){
	if (section_series->points().isEmpty())
		return;

	double new_value = axis_y->limits().first +
					   (axis_y->limits().second - axis_y->limits().first)*value;
	contour_line->setBegin(axis_x->limits().first);
	contour_line->setEnd(axis_x->limits().second);
	contour_line->setValue(new_value);
	contour_line->update();

	hideAllIntersectionLabels();
	intersection_labels.clear();
	if (!intersection_widths.isEmpty() && intersection_widths.first()->isVisible())
		findWidthBetweenIntersectionPoints();
}

void ChartWidget::setContourLineVisible(const bool state){
	contour_line->setVisible(state);
}

void ChartWidget::setInterpolatedDataVisible(const bool state){
	// if state = "true" and current series is not interpolated, interpolate it and show
	if (!section_series->points().isEmpty() && state)
		interpolateSectionSeries();
	interpolated_section_series->setVisible(state);
}

void ChartWidget::setSectionVisible(const bool state){
	section_series->setVisible(state);
}

void ChartWidget::setDecibelScale(const bool state)
{
	csv_handler->decibelScale(state);
	csvSectionAsSeries();
}

void ChartWidget::setIntersectionWidthsVisible(const bool state)
{
	// if state and intersection widths is empty, calculate it
	if (intersection_widths.isEmpty() && state)
		findWidthBetweenIntersectionPoints();

	foreach(auto series, intersection_widths)
		series->setVisible(state);
}

void ChartWidget::setSincSeriesVisible(const bool state)
{
	if (state && sincSeries->points().length() == 0)
		sincApproximation();
	sincSeries->setVisible(state);
}

// finds closest to "point" point in series "series"
QPointF ChartWidget::findClosestPoint(const QPointF &point, QList<QPointF> points)
{
	if (points.isEmpty())
		return QPointF();

	QPointF closestPoint(INT_MAX,INT_MAX);
	QPointF currentPoint;
	double distance = INT_MAX;

	// temporary value to calculate distance between current point and "point"
	double temp;
	double x = point.x();

	int counter = interpolationHelper->segmentOf(x, points);
	// find which of two points of segment is nearest
	for (int j = 0; j < 2; j++){
		currentPoint = points.at(counter + j);
		temp = qSqrt(pow(currentPoint.x() - point.x(),2) +
					 pow(currentPoint.y() - point.y(),2));
		distance = qMin(distance,temp);
	}
	// if temp == distance, last point is closest, else first is closest
	closestPoint = distance == temp ? points.at(counter + 1) : points.at(counter);

	return closestPoint;
}
