#ifndef CSVHANDLER_H
#define CSVHANDLER_H
#include <QWidget>
#include <QtCharts>
#include <QPair>

using namespace QtCharts;

class CSVHandler : public QObject
{
	Q_OBJECT

public:
	CSVHandler(QObject* parent = 0);
	~CSVHandler();
	int openFile(QString file = "");
	void setFile(QString new_filename);
	void setPath(QString new_path);
	void setBasis(const double &new_basis);
	void sectionByRow(const int &number)    {sectionBy(number, false);}
	void sectionByColumn(const int &number) {sectionBy(number, true);}
	void decibelScale(bool state);
	QVector<QPointF> currentSection(){return current_section;}
	QString currentFile(){return m_filename;}
	QString currentPath(){return m_path;}
	QSize dataSize();
	QList<QPair<int, double>> getMaxValues(bool row = true);
	int getCurrentRowNubmer(){return current_row_col.first;}
	int getCurrentColNubmer(){return current_row_col.second;}
	QPair<int,int> currentRowCol() {return current_row_col;}

private:
	void init();
	void sectionBy(const int &number, bool mode);
	void sectionByCurrent();

	bool decibel;
	double basis;
	QString m_path;
	QString m_filename;
	QFileDialog* fileDialog;
	QPair<int,int> current_row_col;
	QVector<QVector<double>> data;
	QVector<QPointF> current_section;
	QList<QPair<int, double>> row_max_values; // contains: [number of line; value]
	QList<QPair<int, double>> col_max_values; // contains: [number of line; value]
signals:
	void fileChanged(QString file);
	void pathChanged(QString path);
};

#endif // CSVHANDLER_H
