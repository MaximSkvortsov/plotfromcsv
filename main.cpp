#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QDebug>
#include <QSettings>
#include <memory>

void GenerateData();

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

	QSettings settings(QSettings::IniFormat, QSettings::UserScope, "zptel", "PlotFromCSV");
	QString language = settings.value("Language", "").toString();	
	if (QString(KSI).size() > 0)
		language = "ru";
	QTranslator appTranslator;
	if (language != "") {
		if (appTranslator.load(QLatin1String("plotfromcsv_") + language.toLatin1(), QLatin1String(":/doc")))
			a.installTranslator(&appTranslator);
		else
			qDebug()<<"Failed while loading translation file";
	}

    MainWindow w;
    w.show();

    return a.exec();
}

