#ifndef VSECTIONFIELD_H
#define VSECTIONFIELD_H

#include <QFrame>
#include <QGridLayout>
#include <QParallelAnimationGroup>
#include <QScrollArea>
#include <QToolButton>
#include <QWidget>

class VSectionField : public QWidget {
	Q_OBJECT
private:

	QGridLayout* mainLayout;
	QToolButton* toggleButton;
	QFrame* headerLine;
	QParallelAnimationGroup* toggleAnimation;
	QScrollArea* contentArea;
	int animationDuration;

public:
	explicit VSectionField(const int animationDuration = 200, QWidget* parent = 0);
	explicit VSectionField(QWidget* parent = 0);

	void setContentLayout(QLayout & contentLayout);
	QAbstractAnimation::State animationState() {return toggleAnimation->state();}
};

#endif // VSECTIONFIELD_H
