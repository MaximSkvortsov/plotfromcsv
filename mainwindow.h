#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
	void open_file_buttonClicked();

    void plotSection();

    void on_saveChartAs_triggered();

    void on_actionPrint_triggered();

    void on_basisSpinBox_valueChanged(double arg1);

    void on_contourLineSpinBox_valueChanged(double arg1);

    void on_actionAntialiasing_triggered();

    void on_actionInterpolated_graph_triggered();

    void on_actionSection_graph_triggered();

    void on_actionIntersection_widths_triggered();

    void on_decibelCheckBox_clicked(bool checked);

    void on_contourLineCheckBox_clicked(bool checked);

    void on_actionShow_all_Width_Labels_triggered();

    void on_actionHide_all_Widths_Labels_triggered();

	void on_actionOpenFile_triggered();

	void on_actionTable_of_sorted_values_toggled(bool arg1);

	void on_actionOpen_axes_editor_triggered();

	void on_actionSinc_approximation_triggered();

	void on_widthCalculatorParameterLineEdit_editingFinished();

	void on_speedLineEdit_editingFinished();

	void on_actionChangeLanguageRussian_triggered();

	void on_actionChangeLanguageEnglish_triggered();

private:
	Ui::MainWindow *ui;
	QWidget * axesEditor;
	void initAxesEditor();
	void replot();
	void resizeEvent(QResizeEvent *event);
	QLayout &makeMaxTableLayout();
	void updateMaxTable(QTableWidget *contentTable);
	void changeLanguage(QString language);
};

#endif // MAINWINDOW_H
