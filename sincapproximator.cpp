#include "sincapproximator.h"

#include <QDebug>
#include <functional>
#include <cmath>

class SincApproximator::Simplex
{
public:
	Simplex(const double eps = 0.01, const double edge = 5, const double delta = 0.5);
	void setAccuracy(const double eps) {m_eps = eps;}
	void setEdgeLength(const double edge) {m_edge = edge;}
	void setDelta(const double delta);

	QVector<double> minimize(QVector<double> first_approx);

	void setFunction(std::function<double(QVector<double>)> func);
	void setPoints(QList<QPointF> points);
	int maxPointIdx() { return m_max_point_idx; }
	QList<QPointF> points() { return m_points; }
	void clear();

private:
	double m_eps;
	double m_edge;
	double m_delta;
	int m_max_point_idx;
	std::function<double(QVector<double>)> m_function;
	QList<QPointF> m_points;

	double squaresDiff(QVector<double> args);
};

SincApproximator::Simplex::Simplex(const double eps,
								   const double edge,
								   const double delta)
	: m_eps(eps), m_edge(edge), m_delta(delta)
{
}

void SincApproximator::Simplex::setDelta(const double delta)
{
	if (delta < 1 && delta > 0)
		m_delta = delta;
}

// Симплексный метод реализован по книге
// В.А. Гончарова "Методы оптимизации", с. 104-110, Москва 2008
// С некоторыми дополнениями. В частности, ребро симплекса для конкретной переменной
// ограничено пятью текущими значениями переменной, так как
// в нашем случае переменные могут отличатся более чем на 3 порядка.

QVector<double> SincApproximator::Simplex::minimize(QVector<double> first_approx)
{
	if (m_points.length() == 0)
		return first_approx;
	// edge of simplex
	double edge = m_edge;
	// number of vertices (number of coordinates + 1)
	int N = first_approx.length() + 1;
	// simplex's vertices - vector of N vertices of N-1 length
	QVector<QVector<double>> simplex;
	simplex.reserve(N);
	simplex.append(first_approx);
	for (int i = 1; i < N; i++)
		simplex.append(QVector<double>(N-1));

	// function for std::sort to sort vertices according to function values from min to max
	std::function<bool(QVector<double>,QVector<double>)> sort_fuction =
			[this](QVector<double> a, QVector<double> b)->bool{return squaresDiff(a) < squaresDiff(b);};

	double diff = INT_MAX;      // current accuracy
	bool edge_changed = true;   // = true for 1st run to calculate the simplex's vertices

	while (diff > m_eps) {
		// calculate new simplex
		if (edge_changed) {
			double coeff = 1;
			for (int i = 1; i < N; i++)
				for (int j = 0; j < N - 1; j++) {
					if (fabs(simplex[0][j]) < edge) {
						coeff = 5 * fabs(simplex[0][j] / edge);
						if (edge < m_edge)
							coeff *= edge/m_edge;
						if (coeff*edge < m_eps)
							coeff = m_eps/edge;
					}
					if (i == j + 1)
						simplex[i][j] = simplex[0][j] + coeff * edge *
										(sqrt(N + 0.0) - 1.0) / ((N - 1.0) * sqrt(2.0));
					else
						simplex[i][j] = simplex[0][j] +  coeff * edge *
										(sqrt(N + 0.0) + N - 2.0) / ((N - 1.0) * sqrt(2.0));
				}

			// sort vertices by function value from min to max
			std::sort(simplex.begin(), simplex.end(), sort_fuction);
			edge_changed = false;
		}

		// try to mirror every vertex until new_val > old_val.
		// Or until the vertices are run out. In that case
		// decrease the edge of the simplex.
		for (int k = N - 1; k > 0; k--) {
			// calculating new vertex (mirrored vertex)
			QVector<double> tmp_vec;
			for (int i = 0; i < N; i++) {
				if (tmp_vec.empty() && i != k) {
					tmp_vec = simplex[i];
					continue;
				}
				if (i != k)
					for (int j = 0; j < N-1; j++)
						tmp_vec[j] += simplex[i][j];
			}

			for (int j = 0; j < N-1; j++) {
				tmp_vec[j] *= 2.0/(N-1.0);
				tmp_vec[j] -= simplex[k][j];
			}

			double old_val = squaresDiff(simplex[k]);
			double new_val = squaresDiff(tmp_vec);

			// if new function value less than old,
			// and difference between new and old values is less than 0.01% of old value,
			// go to next simplex.
			// 0.01% condition is needed to prevent very slow approximation of big function values
			if ( new_val < old_val && (fabs(new_val - old_val)) > fabs(0.0001*old_val)) {
				diff = fabs(new_val - old_val);
				simplex.erase(simplex.begin() + k);
				simplex.append(tmp_vec);
				std::sort(simplex.begin(), simplex.end(), sort_fuction);
				// break current "for" cycle and start again with new simplex if diff is too big
				break;
			} else {
				if (edge < m_eps)
					return simplex[0];
				// if tried out all vertices, set edge = edge/3,
				// calculate new simplex vertices and try again
				if (k == 1) {
					edge *= m_delta;
					edge_changed = true;
				}
			}
		}
	}

	return simplex[0];
}

void SincApproximator::Simplex::setFunction(std::function<double (QVector<double>)> func)
{
	m_function = func;
}

void SincApproximator::Simplex::setPoints(QList<QPointF> points)
{
	if (points.length() == 0)
		return;
	m_points = points;
	QPointF tmp = points[0];
	for (int i = 0; i < points.length(); i++)
		if(tmp.y() < points[i].y()) {
			tmp = points[i];
			m_max_point_idx = i;
		}

}

void SincApproximator::Simplex::clear()
{
	m_points.clear();
	m_max_point_idx = -1;
}

// "args" must not contain "x" value. It must be the same length as your first approximation variable
double SincApproximator::Simplex::squaresDiff(QVector<double> args)
{
	double summ = 0;
	QVector<double> new_args = args;
	double x_max = m_points[m_points.length() - 1].x();
	double x_min = m_points[0].x();
	for (int i = 0; i < m_points.length(); i++) {
		// map x to
		double x = (m_points[i].x() + x_min - m_points[m_max_point_idx].x()) ;
		new_args.append(M_PI * 2 * x / (x_max - x_min));
		double tmp = m_points[i].y() - m_function(new_args);
		summ += tmp*tmp;
		new_args = args;
	}

	return summ;
}

void SincApproximator::run()
{
	QList<QPointF> points = m_simplex->points();

	if (!m_simplex)
		m_simplex = std::unique_ptr<Simplex>(new Simplex());
	m_simplex->setPoints(points);

	if (points.length() == 0)
		return;
	QPointF max_point = points[m_simplex->maxPointIdx()];

	double average_y = 0;
	foreach(auto p, points)
		average_y += p.y();
	average_y /= points.length();

	// modified sinc function. (A - b) * sin(a*x) / (a*x) + b. A = max_point.y(), a,b - variables to minimize.
	// "a" = args[0]
	// "b" = args[1]
	// "x" = args[2]
	// if you want to change the function, remember that "x" MUST be the LAST in list of arguments
	std::function<double (QVector<double>)> func = [max_point](QVector<double> args)->double
	{
		if (args[2] == 0)
			return max_point.y();
		else
			return (max_point.y() - args[1])*sin(args[0]*args[2])/(args[0]*args[2]) + args[1];
	};

	m_simplex->setFunction(func);
	m_simplex->setEdgeLength(0.025*average_y);
	m_simplex->setAccuracy(0.001);

	// init first approximation vector
	QVector<double> first_approx;
	first_approx.append(1);             // "a" first approximation
	first_approx.append(average_y*0.7); // "b" first approximation

	// approximate
	QVector<double> func_args = m_simplex->minimize(first_approx);

	double x_min = points[0].x();
	double x_max = points[points.length() - 1].x();
	// number of points in result series
	int N = ceil((x_max - x_min)/m_step);

	m_result_points.reserve(N);
	// generate seies's points
	for (int i = 0; i < N; i++) {
		// map x to the interval of length 2*PI
		double x = M_PI*2*(i*m_step + x_min - max_point.x()) / (x_max - x_min);
		double y = func(func_args + QVector<double>({x}));
		m_result_points.append(QPointF(i*m_step + x_min,y));
	}

	emit this->resultReady();
}

SincApproximator::SincApproximator(QObject* parent) : QThread(parent)
{

}

SincApproximator::~SincApproximator() = default;

void SincApproximator::setPoints(QList<QPointF> points)
{
	if (!m_simplex)
		m_simplex = std::unique_ptr<Simplex>(new Simplex());
	m_simplex->setPoints(points);
}

void SincApproximator::clear()
{
	if (!m_result_points.isEmpty())
		m_result_points.clear();
	if (m_simplex)
		m_simplex->clear();
}
