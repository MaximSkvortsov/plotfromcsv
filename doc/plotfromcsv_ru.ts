<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ChartWidget</name>
    <message>
        <source>&amp;Units</source>
        <translation type="vanished">Отсчеты</translation>
    </message>
    <message>
        <source>&amp;Amplitude</source>
        <translation type="vanished">Амплитуда</translation>
    </message>
    <message>
        <location filename="../chartwidget/chartwidget.cpp" line="49"/>
        <source>Units</source>
        <translation>Отсчеты</translation>
    </message>
    <message>
        <location filename="../chartwidget/chartwidget.cpp" line="50"/>
        <source>Amplitude</source>
        <translation>Амплитуда</translation>
    </message>
    <message>
        <location filename="../chartwidget/chartwidget.cpp" line="189"/>
        <source>  array</source>
        <translation>  массив</translation>
    </message>
    <message>
        <location filename="../chartwidget/chartwidget.cpp" line="355"/>
        <source>Print graph</source>
        <translation>Печать графика</translation>
    </message>
    <message>
        <location filename="../chartwidget/chartwidget.cpp" line="383"/>
        <source>File name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../chartwidget/chartwidget.cpp" line="383"/>
        <source>Images (*.bmp *.png *.jpg *.jpeg *.ppm *.xbm *.xpm)</source>
        <translation>Изображения</translation>
    </message>
    <message>
        <source>&amp;File name</source>
        <translation type="vanished">&amp;Имя файла</translation>
    </message>
    <message>
        <source>&amp;Images (*.bmp *.png *.jpg *.jpeg *.ppm *.xbm *.xpm)</source>
        <translation type="vanished">&amp;Изображения (*.bmp *.png *.jpg *.jpeg *.ppm *.xbm *.xpm)</translation>
    </message>
    <message>
        <location filename="../chartwidget/chartwidget.cpp" line="547"/>
        <source>X axis parameters:</source>
        <translation>Параметры оси X:</translation>
    </message>
    <message>
        <location filename="../chartwidget/chartwidget.cpp" line="548"/>
        <source>Y axis parameters:</source>
        <translation>Параметры оси Y:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <source>Plot graph from CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="61"/>
        <source>File section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="102"/>
        <source>Decibel scale basis</source>
        <translation>Децибелы. Базис:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="112"/>
        <source>Range</source>
        <translation>Дальность</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="129"/>
        <source>Azimuth</source>
        <translation>Азимут</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="136"/>
        <location filename="../mainwindow.ui" line="340"/>
        <source>Contour line</source>
        <translation>Линия уровня</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="143"/>
        <source>Speed, m/s</source>
        <translation>Скорость, м/c</translation>
    </message>
    <message>
        <source>ADC frequency, MHz</source>
        <translation type="vanished">Частота АЦП</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="153"/>
        <location filename="../mainwindow.cpp" line="200"/>
        <source>ADC frequency, Hz</source>
        <translation>Частота АЦП, Гц</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="203"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="230"/>
        <source>Graph</source>
        <translation>График</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="234"/>
        <source>Intersection widths labels</source>
        <translation>Подписи  линий пересечения</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="245"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="251"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="259"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="263"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="289"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="313"/>
        <source>Save as... (Ctrl + S)</source>
        <translation>Сохранить как... (Ctrl + S)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="318"/>
        <source>Print to... (Ctrl + P)</source>
        <translation>Печать... (Ctrl + P)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="329"/>
        <source>Antialiasing</source>
        <translation>Сглаживание</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="351"/>
        <source>Interpolated graph</source>
        <translation>Интерполированный график</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="362"/>
        <source>Section graph</source>
        <translation>Исходный график</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="373"/>
        <source>Decibels</source>
        <translation>Децибелы</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="384"/>
        <location filename="../mainwindow.cpp" line="52"/>
        <source>Intersection widths</source>
        <translation>Линии пересечения</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="389"/>
        <source>Show all</source>
        <translation>Показать все</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="392"/>
        <source>Show all widths labels</source>
        <translation>Показать все линии пересечения</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="397"/>
        <source>Hide all</source>
        <translation>Скрыть все</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="400"/>
        <source>Hide all widths labels</source>
        <translation>Скрыть все подписи линий пересечения</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="405"/>
        <source>Open (Ctrl + O)</source>
        <translation>Открыть (Ctrl + O)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="416"/>
        <source>Table of sorted values</source>
        <translation>Таблица отсортированных значений</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="421"/>
        <source>Open axes editor</source>
        <translation>Открыть редактор осей</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="424"/>
        <location filename="../mainwindow.cpp" line="57"/>
        <location filename="../mainwindow.cpp" line="327"/>
        <source>Axes editor</source>
        <translation>Редактор осей</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="432"/>
        <source>Sinc approximation</source>
        <translation>Аппроксимация функцией sinc</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="435"/>
        <location filename="../mainwindow.cpp" line="62"/>
        <source>Approximation by sinc function</source>
        <translation>Аппроксимация функцией sinc</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="440"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="445"/>
        <source>Русский</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Interpolation using cubic spline</source>
        <translation type="vanished">Интерполяция кубическим сплайном</translation>
    </message>
    <message>
        <source>&amp;SAR spatial resolution estimator</source>
        <translation type="vanished">Средство оценки пространственного разрешения РСА</translation>
    </message>
    <message>
        <source>&amp;Intersection widths</source>
        <translation type="vanished">&amp;Ширина пересечений линии уровня с графиком</translation>
    </message>
    <message>
        <source>&amp;Axes editor</source>
        <translation type="vanished">&amp;Редактор осей</translation>
    </message>
    <message>
        <source>&amp;Approximation by sinc function</source>
        <translation type="vanished">&amp;Аппроксимация функцией sinc</translation>
    </message>
    <message>
        <source>&amp;Row</source>
        <translation type="vanished">Строка</translation>
    </message>
    <message>
        <source>&amp;Column</source>
        <translation type="vanished">Столбец</translation>
    </message>
    <message>
        <source>&amp;Value</source>
        <translation type="vanished">Значение</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="41"/>
        <source>Interpolation using cubic spline</source>
        <translation>Интерполяция кубическим сплайном</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="48"/>
        <source>SAR spatial resolution estimator</source>
        <translation>Средство оценки пространственного разрешения РСА</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="146"/>
        <source>Row</source>
        <translation>Строка</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="148"/>
        <source>Column</source>
        <translation>Столбец</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="149"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="199"/>
        <source>Row:</source>
        <translation>Строка:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="207"/>
        <source>Column:</source>
        <translation>Столбец:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="208"/>
        <source>PFR, Hz</source>
        <translation>ЧПИ, Гц</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="375"/>
        <source>You need to restart the application to apply changes.</source>
        <translation>Необходимо перезапустить программу, чтобы изменения вступили в силу.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="376"/>
        <source>Language settings changed</source>
        <translation>Настройки языка изменены</translation>
    </message>
    <message>
        <source>&amp;Row:</source>
        <translation type="vanished">Строка:</translation>
    </message>
    <message>
        <source>&amp;ADC frequency, MHz</source>
        <translation type="vanished">Частота АЦП, МГц</translation>
    </message>
    <message>
        <source>&amp;Column:</source>
        <translation type="vanished">Столбец:</translation>
    </message>
    <message>
        <source>&amp;PFR, kHz</source>
        <translation type="vanished">ЧПИ, кГц</translation>
    </message>
</context>
<context>
    <name>QValueAxisReserve</name>
    <message>
        <source>&amp;Min:</source>
        <translation type="vanished">Минимум:</translation>
    </message>
    <message>
        <source>&amp;Max:</source>
        <translation type="vanished">Максимум:</translation>
    </message>
    <message>
        <source>&amp;Grid lines:</source>
        <translation type="vanished">Линии сетки:</translation>
    </message>
    <message>
        <source>&amp;Reset</source>
        <translation type="vanished">Сброс</translation>
    </message>
    <message>
        <location filename="../chartwidget/qvalueaxisreserve.cpp" line="158"/>
        <source>Min:</source>
        <translation>Минимум:</translation>
    </message>
    <message>
        <location filename="../chartwidget/qvalueaxisreserve.cpp" line="159"/>
        <source>Max:</source>
        <translation>Максимум:</translation>
    </message>
    <message>
        <location filename="../chartwidget/qvalueaxisreserve.cpp" line="160"/>
        <source>Grid lines:</source>
        <translation>Линии сетки:</translation>
    </message>
    <message>
        <location filename="../chartwidget/qvalueaxisreserve.cpp" line="164"/>
        <source>Reset</source>
        <translation>Сброс</translation>
    </message>
</context>
</TS>
