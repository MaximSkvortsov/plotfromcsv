#ifndef SCROLLSPINBOX_H
#define SCROLLSPINBOX_H
#include <QSpinBox>

// Simple QSpinBox with one addition - user can use wheel to change value


class ScrollSpinBox : public QSpinBox
{
    Q_OBJECT

public:
    ScrollSpinBox(QWidget* widget = Q_NULLPTR);
    ~ScrollSpinBox();

protected:
    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
};

#endif // SCROLLSPINBOX_H
